package com.example.graduationproject

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.layout_home_dashboard.*

class FragmentHome: Fragment() {

    val user = FirebaseAuth.getInstance().currentUser

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        cardView_history.setOnClickListener {
            val intent = Intent(activity, CollectionHistoryActivity::class.java)
            startActivity(intent)
        }

        cardView_disease.setOnClickListener {
            val intent = Intent(activity, DiseaseDetectorAvtivity::class.java)
            startActivity(intent)
        }
        
        cardView_appIntro.setOnClickListener {
            val intent = Intent(activity, PredictionHistory::class.java)
            startActivity(intent)
        }

        cardView_listOrchard.setOnClickListener {
            val intent = Intent(activity, CalendarActivity::class.java)
            startActivity(intent)
        }


        var name: String
        var id: String
        try{
            if(user!!.displayName.isNullOrEmpty()){
                name = "${user.email}"
                id = "ID: ${user.uid}"
            }else{
                name = "Tên người dùng: ${user.displayName}"
                id = "ID: ${user.uid}"
            }
            user_name.text = name
            user_id.text = id
        }
        catch (e: Exception){
            name = "Tên người dùng: Unknown"
            //id = "ID: Unknown"
            user_name.text = name
            //user_id.text = id
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_home_dashboard, container,false)
    }

}



