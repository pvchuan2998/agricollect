package com.example.graduationproject

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_takephoto.*

class FragmentTakePhoto: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_takephoto, container,false)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

//
//        val connectionManager: ConnectivityManager = this.activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//        val activeNetwork : NetworkInfo? = connectionManager.activeNetworkInfo
//        val isConnected : Boolean = activeNetwork?.isConnectedOrConnecting == true

        buttonEffect(button_takepicwithfruit)
        button_takepicwithfruit.setOnClickListener {
            if(isOnline(requireContext())){
                val intent = Intent(context,CaptuteImageActivity::class.java)
                startActivity(intent)
            }
            else{
                val snack = Snackbar.make(it,"Bạn hiện không kết nối internet!",7000)
                snack.setAction("Kết nối internet", View.OnClickListener {
                    startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
                })
                snack.show()
            }
        }
        buttonEffect(button_takepicwithoutfruit)
        button_takepicwithoutfruit.setOnClickListener {
            if(isOnline(requireContext())){
                val intent = Intent(context,CollectingSensorDataActivity::class.java)
                startActivity(intent)
            }
            else{
                val snack = Snackbar.make(it,"Bạn hiện không kết nối internet!",7000)
                snack.setAction("Kết nối internet", View.OnClickListener {
                    startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
                })
                snack.show()
            }
        }

        buttonEffect(btn_to_iot_connected_activity)
        btn_to_iot_connected_activity.setOnClickListener {
            if(isOnline(requireContext())){
                val intent = Intent(context,IotKitConnectionManagementActivity::class.java)
                startActivity(intent)
            }
            else{
                val snack = Snackbar.make(it,"Bạn hiện không kết nối internet!",7000)
                snack.setAction("Kết nối internet", View.OnClickListener {
                    startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
                })
                snack.show()
            }
        }

    }




    private fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
}