package com.example.graduationproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.graduationproject.data.model.OrchardModel
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import kotlinx.android.synthetic.main.activity_list_orchard.*
import java.util.*

class ListOrchardActivity : AppCompatActivity() {

    private lateinit var viewModel: FirestoreViewModel
    private val arrayList = ArrayList<OrchardModel>()
    private val displayList = ArrayList<OrchardModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_orchard)
        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"
        actionBar.setDisplayHomeAsUpEnabled(true)

        val repository = FirestoreRepository()
        val factory = FirestoreViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(FirestoreViewModel::class.java)

        viewModel.fetchOrchard().observe(this, androidx.lifecycle.Observer { it ->
            arrayList.addAll(it)
            displayList.addAll(arrayList)
            val adapter = ListOrchardAdapter(displayList, this)
            recyclerview_list_orchard.layoutManager = LinearLayoutManager(this)
            recyclerview_list_orchard.adapter = adapter
            pb_loading_list_orchard.visibility = View.GONE
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu!!.findItem(R.id.search)

        if(menuItem != null){
            val searchView = menuItem.actionView as SearchView

            val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            editText.hint = "Tìm kiếm..."

            searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                override fun onQueryTextChange(newText: String?): Boolean {

                    if(newText!!.isNotEmpty()){
                        displayList.clear()
                        val search = newText.toLowerCase(Locale.getDefault())
                        arrayList.forEach{
                            if(it.name.toLowerCase(Locale.getDefault()).contains(search)){
                                displayList.add(it)
                            }
                        }

                        recyclerview_list_orchard.adapter!!.notifyDataSetChanged()
                    }
                    else{
                        displayList.clear()
                        displayList.addAll(arrayList)
                        recyclerview_list_orchard.adapter!!.notifyDataSetChanged()
                    }

                    return true
                }

                override fun onQueryTextSubmit(query: String?): Boolean {

                    return true
                }
            })
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }
}
