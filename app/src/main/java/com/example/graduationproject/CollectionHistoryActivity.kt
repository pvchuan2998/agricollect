package com.example.graduationproject

import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.graduationproject.data.model.CollectionHistory
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_collection_history.*
import kotlinx.android.synthetic.main.news_row_home.*
import kotlinx.android.synthetic.main.news_row_home.view.*
import java.text.SimpleDateFormat

import java.util.*


class CollectionHistoryActivity : AppCompatActivity() {

    private lateinit var viewModel: FirestoreViewModel
    private val arrayList = ArrayList<CollectionHistory>()
    private val displayList = ArrayList<CollectionHistory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collection_history)
        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"
        actionBar.setDisplayHomeAsUpEnabled(true)

        val repository = FirestoreRepository()
        val factory = FirestoreViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(FirestoreViewModel::class.java)

        viewModel.fetchCollectionHistory().observe(this, androidx.lifecycle.Observer {
//            val adapter = GroupAdapter<GroupieViewHolder>()
//            recyclerview_history.adapter = adapter
//            it.forEach {
//                adapter.add(History(it))
//                pb_loading.visibility = View.GONE
//            }
            arrayList.addAll(it)
            displayList.addAll(arrayList)
            val adapter = CollectionHistoryAdapter(displayList, this)
            recyclerview_history.layoutManager = LinearLayoutManager(this)
            recyclerview_history.adapter = adapter
            pb_loading.visibility = View.GONE
        })

    }

//    private fun fetchCollectionHistory(){
//        Log.e("SwitchFarmActivity","fetchFarms function called")
//        val db = FirebaseFirestore.getInstance()
//        val currentUser = FirebaseAuth.getInstance().currentUser
//        val adapter = GroupAdapter<GroupieViewHolder>()
//        recyclerview_history.adapter = adapter
//        if (currentUser != null) {
//            db.collection("CollectionHistory")
//                .whereEqualTo("userId", currentUser.uid)
//                .get().addOnSuccessListener { result ->
//                    val historyList = result.toObjects(CollectionHistory::class.java)
//                    historyList.forEach{
//                        Log.e("SwitchFarmActivity","Loaded document: $it")
//                        //adapter.notifyDataSetChanged()
//                        adapter.add(History(it))
//                        pb_loading.visibility = View.GONE
//                    }
//                }
//        }
//
//    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu!!.findItem(R.id.search)

        if(menuItem != null){
            val searchView = menuItem.actionView as SearchView

            val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            editText.hint = "Tìm kiếm..."

            searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                override fun onQueryTextChange(newText: String?): Boolean {

                    if(newText!!.isNotEmpty()){
                        displayList.clear()
                        val search = newText.toLowerCase(Locale.getDefault())
                        arrayList.forEach{
                            if(it.orchardName.toLowerCase(Locale.getDefault()).contains(search)){
                                displayList.add(it)
                            }
                        }

                        recyclerview_history.adapter!!.notifyDataSetChanged()
                    }
                    else{
                        displayList.clear()
                        displayList.addAll(arrayList)
                        recyclerview_history.adapter!!.notifyDataSetChanged()
                    }

                    return true
                }

                override fun onQueryTextSubmit(query: String?): Boolean {

                    return true
                }
            })
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }
}

//class History(private val history: CollectionHistory): Item<GroupieViewHolder>(){
//    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
//
//        val textID: String = "Mã nhân viên: ${history.userId}"
//        val textTime: String = "Thời gian: ${history.timeStamp.toString()}"
//        val textName: String = "Tên vườn: ${history.orchardName}"
//        val textAirTemp: String = "Nhiệt độ không khí: ${history.air_temp} (oC)"
//        val textSoilTemp: String = "Nhiệt độ đất: ${history.soil_temp} (oC)"
//        val textAirHumid: String = "Độ ẩm không khí: ${history.air_humid} (%)"
//        val textSoilHumid: String = "Độ ẩm đất: ${history.soil_humid} (%)"
//        val textEC: String = "EC: ${history.ec} (mS/cm)"
//        val textPH: String = "PH: ${history.ph}"
//
//        viewHolder.itemView.textview_staffId.text = textID
//
//        viewHolder.itemView.textview_time.text = textTime
//
//        viewHolder.itemView.textview_orchardName.text = textName
//
//        viewHolder.itemView.tv_air_temp.text = textAirTemp
//
//        viewHolder.itemView.tv_air_humid.text = textAirHumid
//
//        viewHolder.itemView.tv_soil_humid.text = textSoilHumid
//
//        viewHolder.itemView.tv_soil_temp.text = textSoilTemp
//
//        viewHolder.itemView.tv_ec.text = textEC
//
//        viewHolder.itemView.tv_ph.text = textPH
//    }
//
//
//    override fun getLayout(): Int {
//        return R.layout.news_row_home
//    }
//}
