package com.example.graduationproject

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.graduationproject.data.model.DiseaseDetail
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_disease_details.*


class DiseaseDetails : AppCompatActivity() {

    companion object {
        var diseaseName: String ? = null
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        //window.requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disease_details)

        val actionBar = supportActionBar
        //actionBar!!.setBackgroundDrawable(ColorDrawable(R.color.graylight))
        //actionBar.elevation = 0F
        actionBar!!.title = ""
        actionBar.setDisplayHomeAsUpEnabled(true)

        fetchDiseaseDetails()
    }

    private fun fetchDiseaseDetails() {
        val db = FirebaseFirestore.getInstance()
        val sentence = diseaseName!!.replace("\\s".toRegex(), "")
        Log.e("Disease", sentence)
        db.collection("DiseaseDetails").document(sentence)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val doc = document.toObject(DiseaseDetail::class.java)
                    try{
                        //val name: String = "${doc!!.name} \n ${doc.vnName}"
                        Picasso.get()
                            .load(doc!!.imageUrl)
                            .into(imv_diseaseImg)
                        tv_diseaseName.text = doc.name
                        tv_diseaseVNName.text = doc.vnName
                        tv_disease_causes_details.text = doc.causes
                        tv_disease_symptoms_details.text = doc.details
                        tv_disease_options_details.text = doc.control
                        tv_disease_biologicalControl.text = doc.biologicalControl
                        tv_disease_chemicalControl.text = doc.chemicalControl
                        Log.d("Data", "DocumentSnapshot data: ${document.data}")
                    }catch (e: NullPointerException){
                        tv_diseaseVNName.text = "Hiện chưa có thông tin"
                        tv_diseaseName.text = "Hiện chưa có thông tin"
                        tv_disease_causes_details.text = "Hiện chưa có thông tin"
                        tv_disease_symptoms_details.text = "Hiện chưa có thông tin"
                        tv_disease_options_details.text = "Hiện chưa có thông tin"
                        tv_disease_biologicalControl.text = "Hiện chưa có thông tin"
                        tv_disease_chemicalControl.text = "Hiện chưa có thông tin"
                        Log.d("Data", "No such document")
                    }
                } else {
                    val name: String = "Tên loại bệnh: Unknown"
                    tv_diseaseName.text = name
                    tv_disease_causes_details.text = ""
                    tv_disease_symptoms_details.text = ""
                    tv_disease_options_details.text = ""
                    Log.d("Data", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("Data", "Get failed with ", exception)
            }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
