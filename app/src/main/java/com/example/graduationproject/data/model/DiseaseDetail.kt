package com.example.graduationproject.data.model

data class DiseaseDetail(
    var name: String,
    var details: String,
    var control: String,
    var imageUrl: String,
    var causes: String,
    var label: String,
    var vnName: String,
    var chemicalControl: String,
    var biologicalControl: String
) {
    constructor():this("", "", "", "", "", "", "", "", "")
}