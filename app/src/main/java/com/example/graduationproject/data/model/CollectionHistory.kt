package com.example.graduationproject.data.model

import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class CollectionHistory(
    var userId: String,
    var orchardName: String,
    var soil_temp: String,
    var air_temp: String,
    var soil_humid: String,
    var air_humid: String,
    var ec: String,
    var ph: String,
    @ServerTimestamp
    var timeStamp: Date? = null
) {
    constructor():this("","", "", "", "", "", "", "")
}