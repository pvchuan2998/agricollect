package com.example.graduationproject.data.repositories

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.graduationproject.Event
import com.example.graduationproject.MainActivity
import com.example.graduationproject.data.model.CollectionHistory
import com.example.graduationproject.data.model.DiseaseResultModel
import com.example.graduationproject.data.model.KetQuaDo
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.Exception

//
class FirestoreRepository {
    val TAG = "FIREBASE_REPOSITORY"
    var firestoreDB = FirebaseFirestore.getInstance()


    //Lấy lịch sử thu thập thông tin môi trường từ Firestore
    fun fetchCollectionHistory(): CollectionReference{
        return firestoreDB.collection("CollectionHistory")
    }

    //Lưu kết quả thu thập thông tin môi trường vào Firebase
    fun saveDataPackage(model: KetQuaDo, context: Context){

            var documentReference = firestoreDB.collection("KetQuaDo").add(model)
                .addOnSuccessListener { documentReference ->
                    //context.toast("Lưu dữ liệu thành công!")
                    try{
                    context.showDialogOK("Lưu dữ liệu thành công",
                        DialogInterface.OnClickListener { _, which ->
                            when(which){
                                DialogInterface.BUTTON_POSITIVE -> context.moveToMainActivity()
                            }
                        })
                    }
                    catch (e: Exception){
                        Log.d("Error", e.message.toString())
                        context.toast("Lưu dữ liệu thành công!")
                    }
                }
                .addOnFailureListener { e ->
                    context.toast("Lưu dữ liệu thất bại!")
                }

//        var documentReference = firestoreDB.collection("KetQuaDo").add(model)
//            .addOnSuccessListener { documentReference ->
//                //context.toast("Lưu dữ liệu thành công!")
//                context.showDialogOK("Lưu dữ liệu thành công",
//                    DialogInterface.OnClickListener { _, which ->
//                        when(which){
//                            DialogInterface.BUTTON_POSITIVE -> context.moveToMainActivity()
//                        }
//                    })
//            }
//            .addOnFailureListener { e ->
//                context.toast("Lưu dữ liệu thất bại!")
//            }
    }

    private fun Context.moveToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        //finish()
    }

    fun Context.toast(message: CharSequence) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    fun Context.showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("Đồng ý", okListener)
            .create()
            .show()
    }

    fun saveEvent(event: Event, context: Context){
        val ref = FirebaseFirestore.getInstance()
        ref.collection("Event").add(event)
            .addOnSuccessListener { documentReference ->
                context.toast( "Lưu sự kiện thành công!")
            }
            .addOnFailureListener { e ->
                context.toast( "Lưu sự kiện thất bại!")
                context.moveToMainActivity()
            }
    }


    fun fetchPredictionHistory(): CollectionReference{
        return firestoreDB.collection("DiseasesDetectorResults")
    }

    fun fetchEvent(): CollectionReference{
        return firestoreDB.collection("Event")
    }

    fun fetchDiseaseData(): CollectionReference{
        return firestoreDB.collection("DiseaseDetails")
    }

    fun fetchOrchard(): CollectionReference{
        return firestoreDB.collection("Orchard")
    }
}

