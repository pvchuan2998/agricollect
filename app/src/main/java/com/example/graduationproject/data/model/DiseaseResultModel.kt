package com.example.graduationproject.data.model

import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class DiseaseResultModel(
    var uid: String,
    var imgUri: String,
    var infectedAreaImgUri: String,
    var infectedArea: String,
    var lat: String,
    var long: String,
    var diagnosedDisease: String,
    var orchardId: String,
    var plant: String,
    @ServerTimestamp
    var time: Date? = null
) {
    constructor():this("","","","","", "", "", "", "")
}