package com.example.graduationproject.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

@Parcelize
data class KetQuaDo(
    var uid: String,
    var maSoKhuVuon: String,
    var packageId: String,
    var loaiTrai: String,
    var doAmDat: Double,
    var nhietDoDat: Double,
    var doAmKK: Double,
    var nhietDoKK: Double,
    var doECDat: Double,
    var doPHDat: Double,
    var kichThuocTrai_Dai: Double,
    var kichThuocTrai_Rong: Double,
    var longtitude: String,
    var latitude: String,
    @ServerTimestamp
    var thoiGianGui: Date ? = null
): Parcelable {
    constructor():this("","","","", 1.0,1.0,1.0,
        1.0,1.0,1.0,0.0,0.0,
        "0.0","0.0")
}