package com.example.graduationproject.data.repositories

import com.example.graduationproject.data.firebase.FirebaseSource

class UserRepository(private val firebase: FirebaseSource) {

    fun login(email: String, password: String) = firebase.login(email,password)

    fun register(email: String, password: String) = firebase.register(email,password)

    fun logout() = firebase.logout()

    fun currentUser() = firebase.currentUser()
}