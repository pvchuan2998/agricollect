package com.example.graduationproject.data.model

class SensorData(
    var temp: Double,
    var humid: Double,
    var ph: Double,
    var time: Int
) {
    constructor():this(0.0, 0.0, 0.0, 0)
}