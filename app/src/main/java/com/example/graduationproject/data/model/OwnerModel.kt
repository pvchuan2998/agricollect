package com.example.graduationproject.data.model

data class OwnerModel(
    var name: String,
    var address: String,
    var phone: String
) {
    constructor():this("", "", "")
}