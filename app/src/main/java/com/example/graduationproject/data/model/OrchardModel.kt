package com.example.graduationproject.data.model

data class OrchardModel(
    var id: String,
    var address: String,
    var area: String,
    var name: String,
    var owner: String,
    var type: String
) {
    constructor():this("", "", "", "", "", "")
}