package com.example.graduationproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.graduationproject.adapter.IotKitAdapter
import com.hcmus.k16101353149.iotcore.connectivity.IotKitConnectivity
import com.hcmus.k16101353149.iotcore.sensor.SensorManager
import kotlinx.coroutines.*

class IotKitConnectionManagementActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        private const val TAG = "ConnectedIoTSensor"
    }

    private var mIsFinding = false
    private lateinit var mIotKitConnectivity: IotKitConnectivity

    private lateinit var rvIotKit: RecyclerView
    private lateinit var btFindIotKit: Button

    private lateinit var mIotKitAdapter: IotKitAdapter

    private val mSensorManagerListener = object : SensorManager.SensorConnectionListener {
        override fun onSensorConnect(clientId: String, username: String) {
            Log.d(TAG, "onSensorConnect - START")
            CoroutineScope(Dispatchers.Main).launch {
                mIotKitAdapter.notifyDataSetChanged()
            }
        }

        override fun onSensorDisconnect(clientId: String, username: String) {
            Log.d(TAG, "onSensorDisconnect - START")
            CoroutineScope(Dispatchers.Main).launch {
                mIotKitAdapter.notifyDataSetChanged()
            }
        }

        override fun onSensorConnectionLost(clientId: String, username: String) {
            Log.d(TAG, "onSensorConnectionLost - START")
            CoroutineScope(Dispatchers.Main).launch {
                mIotKitAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iot_kit_connection_management)

        initView()

        AgriCollectApplication.startSensorListener(mSensorManagerListener)

        mIotKitConnectivity = IotKitConnectivity(this@IotKitConnectionManagementActivity)
    }

    private fun initView() {
        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"
        actionBar.setDisplayHomeAsUpEnabled(true)

        rvIotKit = findViewById(R.id.rvIotKit)
        btFindIotKit = findViewById(R.id.btFindIotKit)

        mIotKitAdapter = IotKitAdapter(AgriCollectApplication.getIotKitConnectionList())
        rvIotKit.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvIotKit.adapter = mIotKitAdapter

        btFindIotKit.setOnClickListener(this@IotKitConnectionManagementActivity)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btFindIotKit -> {
                Log.d(TAG, "onClick - btFindIotKit")
                if(mIsFinding) {
                    mIotKitConnectivity.stopSendingMulticastLocalIP()
                    btFindIotKit.text = "Tìm AgriKit Đang Dùng Cùng WiFi"
                } else {
                    mIotKitConnectivity.startSendingMulticastLocalIP()
                    btFindIotKit.text = "Đang tìm kiếm...\nBấm lần nữa để dừng"
                }
                mIsFinding = !mIsFinding
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mIotKitConnectivity.stopSendingMulticastLocalIP()
        AgriCollectApplication.stopSensorListener(mSensorManagerListener)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
