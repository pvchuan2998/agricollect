package com.example.graduationproject

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.graduationproject.data.model.CollectionHistory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.news_row_home.view.*

class CollectionHistoryAdapter(val arrayList: ArrayList<CollectionHistory>, val context: Context) :
    RecyclerView.Adapter<CollectionHistoryAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindItems(history: CollectionHistory){
            val textID: String = "Mã nhân viên: ${history.userId}"
            val textTime: String = "Thời gian: ${history.timeStamp.toString()}"
            val textName: String = "Tên vườn: ${history.orchardName}"
            val textAirTemp: String = "Nhiệt độ không khí: ${history.air_temp} (oC)"
            val textSoilTemp: String = "Nhiệt độ đất: ${history.soil_temp} (oC)"
            val textAirHumid: String = "Độ ẩm không khí: ${history.air_humid} (%)"
            val textSoilHumid: String = "Độ ẩm đất: ${history.soil_humid} (%)"
            val textEC: String = "EC: ${history.ec} (mS/cm)"
            val textPH: String = "PH: ${history.ph}"

            itemView.textview_staffId.text = textID

            itemView.textview_time.text = textTime

            itemView.textview_orchardName.text = textName

            itemView.tv_air_temp.text = textAirTemp

            itemView.tv_air_humid.text = textAirHumid

            itemView.tv_soil_humid.text = textSoilHumid

            itemView.tv_soil_temp.text = textSoilTemp

            itemView.tv_ec.text = textEC

            itemView.tv_ph.text = textPH
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.news_row_home, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(arrayList[position])

    }
}