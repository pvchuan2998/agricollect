package com.example.graduationproject

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.graduationproject.data.firebase.FirebaseSource
import com.example.graduationproject.ui.auth.LoginActivity
import kotlinx.android.synthetic.main.fragment_settings.*

class FragmentSettings: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonEffect(btn_menu_logout)
        btn_menu_logout.setOnClickListener {
            val firebaseSource = FirebaseSource()
            firebaseSource.logout()
            val intent = Intent(this.context, LoginActivity::class.java)
            startActivity(intent)
        }

        buttonEffect(btn_menu_profile)
        btn_menu_profile.setOnClickListener {
            val intent = Intent(this.context, UserProfile::class.java)
            startActivity(intent)
        }

        buttonEffect(btn_list_orchard)
        btn_list_orchard.setOnClickListener {
            val intent = Intent(this.context, ListOrchardActivity::class.java)
            startActivity(intent)
        }

        buttonEffect(btn_menu_intro)
        btn_menu_intro.setOnClickListener {
            val intent = Intent(this.context, AppIntroActivity::class.java)
            startActivity(intent)
        }
    }



    private fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }

}