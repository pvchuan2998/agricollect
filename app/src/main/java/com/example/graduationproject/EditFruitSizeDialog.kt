package com.example.graduationproject

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import kotlinx.android.synthetic.main.dialog_edit_fruit_size.*

class EditFruitSizeDialog(context: Context, var dialogListener: AddDialogEditSizeListener):AppCompatDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_edit_fruit_size)

        button_ok_fruit_size.setOnClickListener {
            val w = editText_width.text.toString()
            val h = editText_height.text.toString()
            dialogListener.onOkButtonSizeClicked(w,h)
            dismiss()
        }

        button_cancel_fruit_size.setOnClickListener {
            cancel()
        }


    }
}