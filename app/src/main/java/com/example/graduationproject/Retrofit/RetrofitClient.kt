package com.example.graduationproject.Retrofit

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

class RetrofitClient(private val mUrl: String) {
    private var retrofitClient: Retrofit? = null

    val client: Retrofit
        get() {
            if (retrofitClient == null) {
                retrofitClient = Retrofit.Builder()
                        .baseUrl(mUrl)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .build()
            }
            return retrofitClient!!
        }
}
