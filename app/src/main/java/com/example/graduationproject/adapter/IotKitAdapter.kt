package com.example.graduationproject.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.graduationproject.R
import com.hcmus.k16101353149.iotcore.sensor.SensorManager

class IotKitAdapter(private val mIotKitConnectionList: HashMap<String, SensorManager.ConnectionStatus>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return IotKitConnectionViewHolder(layoutInflater.inflate(R.layout.item_connection_iot_kit, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (itemCount > position && position != RecyclerView.NO_POSITION) {
            val iotKitConnectionViewHolder = holder as IotKitConnectionViewHolder
            val key = mIotKitConnectionList.keys.toList()[position]
            val value = mIotKitConnectionList[key] ?: SensorManager.ConnectionStatus.NONE
            iotKitConnectionViewHolder.build(Pair(key, value))
        }
    }

    override fun getItemCount(): Int = mIotKitConnectionList.size

    internal class IotKitConnectionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val tvIotKitName: TextView = view.findViewById(R.id.tvIotKitName)
        private val tvIotKitStatus: TextView = view.findViewById(R.id.tvIotKitStatus)

        fun build(pair: Pair<String, SensorManager.ConnectionStatus>) {
            tvIotKitName.text = pair.first
            tvIotKitStatus.text = SensorManager.ConnectionStatus.getStatusStr(pair.second)
        }
    }
}