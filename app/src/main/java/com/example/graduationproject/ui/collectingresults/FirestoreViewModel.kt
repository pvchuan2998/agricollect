package com.example.graduationproject.ui.collectingresults

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.graduationproject.Event
import com.example.graduationproject.data.model.*
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot

class FirestoreViewModel(private val repository: FirestoreRepository): ViewModel() {
    val TAG = "FIRESTORE_VIEW_MODEL"
    var firebaseRepository = FirestoreRepository()


    var savedCollectionHistory : MutableLiveData<List<CollectionHistory>> = MutableLiveData()


    var savedPredictionHistory : MutableLiveData<List<DiseaseResultModel>> = MutableLiveData()


    var savedEvent : MutableLiveData<List<Event>> = MutableLiveData()

    var savedDiseases : MutableLiveData<List<DiseaseDetail>> = MutableLiveData()

    var savedOrchards : MutableLiveData<List<OrchardModel>> = MutableLiveData()


    //Lưu kết quả thu thập thông tin môi trường vào Firebase
    fun saveDataPackage(model: KetQuaDo, context: Context){
        repository.saveDataPackage(model, context)
    }

    //Lấy lịch sử thu thập thông tin môi trường từ Firestore
    fun fetchCollectionHistory(): LiveData<List<CollectionHistory>> {
        val currentUser = FirebaseAuth.getInstance().currentUser
        firebaseRepository.fetchCollectionHistory()
            .whereEqualTo("userId", currentUser!!.uid)
            .orderBy("timeStamp", Query.Direction.DESCENDING)
            .addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                savedCollectionHistory.value = null
                return@EventListener
            }
            val savedHistoryList : MutableList<CollectionHistory> = mutableListOf()
            for (doc in value!!) {
                val historyItem = doc.toObject(CollectionHistory::class.java)
                Log.e("CollectionActivity","Loaded document: $historyItem")
                savedHistoryList.add(historyItem)
            }
            savedCollectionHistory.value = savedHistoryList
        })

        return savedCollectionHistory
    }


    fun fetchPredictionHistory(): LiveData<List<DiseaseResultModel>> {
        val currentUser = FirebaseAuth.getInstance().currentUser
        firebaseRepository.fetchPredictionHistory()
            .whereEqualTo("uid", currentUser!!.uid)
            .orderBy("time", Query.Direction.DESCENDING)
            .addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    savedPredictionHistory.value = null
                    return@EventListener
                }

                val savedHistoryList : MutableList<DiseaseResultModel> = mutableListOf()
                for (doc in value!!) {
                    val historyItem = doc.toObject(DiseaseResultModel::class.java)
                    savedHistoryList.add(historyItem)
                }
                savedPredictionHistory.value = savedHistoryList
            })

        return savedPredictionHistory
    }

    fun fetchEvent(): LiveData<List<Event>> {
        val currentUser = FirebaseAuth.getInstance().currentUser
        firebaseRepository.fetchEvent()
            .whereEqualTo("uid", currentUser!!.uid)
            .addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    savedEvent.value = null
                    return@EventListener
                }

                val savedHistoryList : MutableList<Event> = mutableListOf()
                for (doc in value!!) {
                    val historyItem = doc.toObject(Event::class.java)
                    savedHistoryList.add(historyItem)
                }
                savedEvent.value = savedHistoryList
            })

        return savedEvent
    }

    fun fetchDiseaseData(): LiveData<List<DiseaseDetail>> {
        firebaseRepository.fetchDiseaseData()
            .addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                if (e != null) {
                    savedDiseases.value = null
                    return@EventListener
                }

                val savedList : MutableList<DiseaseDetail> = mutableListOf()
                for (doc in value!!) {
                    val diseaseItem = doc.toObject(DiseaseDetail::class.java)
                    savedList.add(diseaseItem)
                }
                savedDiseases.value = savedList
            })

        return savedDiseases
    }

    fun saveEvent(model: Event, context: Context){
        repository.saveEvent(model, context)
    }

   fun fetchOrchard(): LiveData<List<OrchardModel>>{
       firebaseRepository.fetchOrchard()
           .addSnapshotListener(EventListener<QuerySnapshot>{ value, e ->
               if(e != null){
                   savedOrchards.value = null
                   return@EventListener
               }

               var savedList : MutableList<OrchardModel> = mutableListOf()
               for(doc in value!!){
                   val item = doc.toObject(OrchardModel::class.java)
                   savedList.add(item)
               }
               savedOrchards.value = savedList
           })
       return savedOrchards
   }

}