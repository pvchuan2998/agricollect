package com.example.graduationproject.ui.home

import android.view.View
import androidx.lifecycle.ViewModel
import com.example.graduationproject.data.repositories.UserRepository
import com.example.graduationproject.utils.startLoginActivity

class HomeViewModel(
    private val repository: UserRepository
) : ViewModel() {

    val user by lazy {
        repository.currentUser()
    }

    fun logout(view: View){
        repository.logout()
        view.context.startLoginActivity()
    }
}