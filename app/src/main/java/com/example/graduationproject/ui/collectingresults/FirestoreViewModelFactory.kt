package com.example.graduationproject.ui.collectingresults

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.graduationproject.data.repositories.FirestoreRepository

@Suppress("UNCHECKED_CAST")
class FirestoreViewModelFactory(private val repository: FirestoreRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FirestoreViewModel(repository) as T
    }
}