package com.example.graduationproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.graduationproject.data.model.OrchardModel
import com.example.graduationproject.data.model.OwnerModel
import com.example.graduationproject.data.model.SensorData
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_orchard_detail.*

class OrchardDetailActivity : AppCompatActivity() {

    companion object {
        var orchardId: String? = null
        var ownerId: String? = null

    }

    //Creating member variables
    private var mFirebaseDatabase: DatabaseReference?=null
    private var mFirebaseInstance: FirebaseDatabase?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orchard_detail)
        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"
        actionBar.setDisplayHomeAsUpEnabled(true)


        //Getting instances of FirebaseDatabase
        mFirebaseInstance= FirebaseDatabase.getInstance()

        //get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance!!.getReference("/${orchardId}")

        fetchDiseaseDetails()
        fetchOwner()
        //fetchRealtimeSensorData()

//        switch_notification?.setOnCheckedChangeListener { _, isChecked ->
//            val message = if (isChecked) "ON" else "OFF"
//            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
//            if(isChecked){
//                enableNotificationFlag = true
//            }
//        }
    }

    private fun fetchDiseaseDetails() {
        val db = FirebaseFirestore.getInstance()

        db.collection("Orchard").document(orchardId!!)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    pb_loading_orchard_detail.visibility = View.GONE
                    val doc = document.toObject(OrchardModel::class.java)
                    val name: String = "Tên vườn: ${doc!!.name}"
                    val address: String = "Địa chỉ vườn: ${doc.address}"
                    val type: String = "Loại đất canh tác: ${doc.type}"
                    val area: String = "Diện tích vườn: ${doc.area} (ha)"

                    tv_detail_name.text = name
                    tv_detail_address.text = address
                    tv_detail_type.text = type
                    tv_detail_area.text = area

                    Log.d("Data", "DocumentSnapshot data: ${document.data}")
                } else {
                    Log.d("Data", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("Data", "Get failed with ", exception)
            }
    }

    private fun fetchOwner(){
        val db = FirebaseFirestore.getInstance()

        db.collection("Owner").document(ownerId!!)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val doc = document.toObject(OwnerModel::class.java)
                    val name: String = "Tên chủ vườn: ${doc!!.name}"
                    val address: String = "Địa chỉ chủ vườn: ${doc.address}"
                    val phone: String = "Điện thoại chủ vườn: ${doc.phone}"


                    tv_detail_owner_name.text = name
                    tv_detail_owner_address.text = address
                    tv_detail_owner_phone.text = phone


                    Log.d("Data", "DocumentSnapshot data: ${document.data}")
                } else {
                    Log.d("Data", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("Data", "Get failed with ", exception)
            }
    }

    private fun fetchRealtimeSensorData() {
        mFirebaseDatabase!!.child("airSensor").limitToLast(2)
            .addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.w("DBError", "loadPost:onCancelled", error.toException())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val list : MutableList<SensorData> = mutableListOf()
                val children = snapshot.children
                children.forEach {
                    list.add(it.getValue(SensorData::class.java)!!)
                }
                Log.e("data", list[list.lastIndex].temp.toString())
                val textTemp = list[list.lastIndex].temp.toString()
                val textHumid = list[list.lastIndex].humid.toString()

                when {
                    list[list.lastIndex].temp > 30 -> {
                        tvAirTempStatus.text = "Cao"
                        tvAirTempStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    list[list.lastIndex].temp < 20 -> {
                        tvAirTempStatus.text = "Thấp"
                        tvAirTempStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    else -> {
                        tvAirTempStatus.text = "Bình thường"
                        tvAirTempStatus.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                    }
                }
                when {
                    list[list.lastIndex].humid > 80 -> {
                        tvAirHumidStatus.text = "Cao"
                        tvAirHumidStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    list[list.lastIndex].humid < 50 -> {
                        tvAirHumidStatus.text = "Thấp"
                        tvAirHumidStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    else -> {
                        tvAirHumidStatus.text = "Bình thường"
                        tvAirHumidStatus.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                    }
                }
                tvAirTemp.text =  "${textTemp} \u2103"
                tvAirHumid.text = "${textHumid} %"
            }
        })

        mFirebaseDatabase!!.child("soilSensor").limitToLast(2)
            .addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.w("DBError", "loadPost:onCancelled", error.toException())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val list : MutableList<SensorData> = mutableListOf()
                val children = snapshot.children
                children.forEach {
                    list.add(it.getValue(SensorData::class.java)!!)
                }
                val textTemp = list[list.lastIndex].temp.toString()
                val textHumid = list[list.lastIndex].humid.toString()

                when {
                    list[list.lastIndex].temp > 30 -> {
                        tvSoilTempStatus.text = "Cao"
                        tvSoilTempStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    list[list.lastIndex].temp < 15 -> {
                        tvSoilTempStatus.text = "Thấp"
                        tvSoilTempStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    else -> {
                        tvSoilTempStatus.text = "Bình thường"
                        tvSoilTempStatus.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                    }
                }
                when {
                    list[list.lastIndex].humid > 70 -> {
                        tvSoilHumidStatus.text = "Cao"
                        tvSoilHumidStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    list[list.lastIndex].humid < 50 -> {
                        tvSoilHumidStatus.text = "Thấp"
                        tvSoilHumidStatus.setTextColor(resources.getColor(R.color.warning))
                    }
                    else -> {
                        tvSoilHumidStatus.text = "Bình thường"
                        tvSoilHumidStatus.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                    }
                }
                tvSoilTemp.text =  "${textTemp} \u2103"
                tvSoilHumid.text = "${textHumid} %"
            }
        })

        mFirebaseDatabase!!.child("phSensor").limitToLast(2)
            .addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.w("DBError", "loadPost:onCancelled", error.toException())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val list : MutableList<SensorData> = mutableListOf()
                val children = snapshot.children
                children.forEach {
                    list.add(it.getValue(SensorData::class.java)!!)
                }

                val text = list[list.lastIndex].ph.toString()
                if(list[list.lastIndex].ph > 7){
                    tvPHStatus.text = "Cao"
                    tvPHStatus.setTextColor(resources.getColor(R.color.warning))
                }
                else if(list[list.lastIndex].ph < 6){
                    tvPHStatus.text = "Thấp"
                    tvPHStatus.setTextColor(resources.getColor(R.color.warning))
                }
                else{
                    tvPHStatus.text = "Bình thường"
                    tvPHStatus.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                }
                tvpH.text =  "${text}"

            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
