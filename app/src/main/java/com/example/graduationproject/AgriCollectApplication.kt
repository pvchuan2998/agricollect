package com.example.graduationproject

import android.app.Application
import android.util.Log
import android.widget.Toast
import com.example.graduationproject.data.firebase.FirebaseSource
import com.example.graduationproject.data.repositories.UserRepository
import com.example.graduationproject.ui.auth.AuthViewModelFactory
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.example.graduationproject.ui.home.HomeViewModelFactory
import com.google.firebase.messaging.FirebaseMessaging
import com.hcmus.k16101353149.iotcore.sensor.IoTSensor
import com.hcmus.k16101353149.iotcore.sensor.SensorManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class AgriCollectApplication : Application(), KodeinAware {
    companion object {
        private const val TAG = "AgriCollectApplication"

        private lateinit var mSensorManager: SensorManager

        fun startSensorListener(listener: SensorManager.SensorListener) {
            mSensorManager.startSensorListener(listener)
        }

        fun stopSensorListener(listener: SensorManager.SensorListener) {
            mSensorManager.stopSensorListener(listener)
        }

        fun getIotKitConnectionList(): HashMap<String, SensorManager.ConnectionStatus> = mSensorManager.mIotKitConnectionList

        //var enableNotificationFlag: Boolean = false
    }

    private val mSensorManagerListener = object : SensorManager.SensorConnectionListener {
        override fun onSensorConnect(clientId: String, username: String) {
            Log.d(TAG, "onSensorConnect - START")
            CoroutineScope(Dispatchers.Main).launch {
                val value = SensorManager.ConnectionStatus.getStatusStr(
                        getIotKitConnectionList()[clientId] ?: SensorManager.ConnectionStatus.NONE)
                Toast.makeText(applicationContext, "$clientId - $value", Toast.LENGTH_SHORT).show()
            }
        }

        override fun onSensorDisconnect(clientId: String, username: String) {
            Log.d(TAG, "onSensorDisconnect - START")
            CoroutineScope(Dispatchers.Main).launch {
                val value = SensorManager.ConnectionStatus.getStatusStr(
                        getIotKitConnectionList()[clientId] ?: SensorManager.ConnectionStatus.NONE)
                Toast.makeText(applicationContext, "$clientId - $value", Toast.LENGTH_SHORT).show()
            }
        }

        override fun onSensorConnectionLost(clientId: String, username: String) {
            Log.d(TAG, "onSensorConnectionLost - START")
        }
    }

    override val kodein = Kodein.lazy {
        import(androidXModule(this@AgriCollectApplication))

        bind() from singleton { FirebaseSource() }
        bind() from singleton { UserRepository(instance()) }
        bind() from provider { AuthViewModelFactory(instance()) }
        bind() from provider { HomeViewModelFactory(instance()) }
        bind() from provider { FirestoreViewModelFactory(instance()) }
    }

    override fun onCreate() {
        Log.d(TAG, "onCreate - START")
        super.onCreate()
        mSensorManager = SensorManager(arrayListOf(
                IoTSensor.AIR_HUMID,
                IoTSensor.AIR_TEMP,
                IoTSensor.SOIL_HUMID,
                IoTSensor.SOIL_TEMP,
                IoTSensor.SOIL_PH,
                IoTSensor.SOIL_EC
        ))
        mSensorManager.startServer()
        startSensorListener(mSensorManagerListener)
//        if(enableNotificationFlag){
//
//        }
        FirebaseMessaging.getInstance().subscribeToTopic("pushNotificationsAirHumidity")
        FirebaseMessaging.getInstance().subscribeToTopic("pushNotificationsAirTemperature")
        FirebaseMessaging.getInstance().subscribeToTopic("pushNotificationsPH")
        FirebaseMessaging.getInstance().subscribeToTopic("pushNotificationsSoilHumidity")
        FirebaseMessaging.getInstance().subscribeToTopic("pushNotificationsSoilTemperature")

    }
}