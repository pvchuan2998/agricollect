package com.example.graduationproject

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.PorterDuff
import android.graphics.drawable.BitmapDrawable
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.*
//import android.support.annotation.RequiresApi
//import android.support.v7.app.AppCompatActivity
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.example.graduationproject.DiseasesDetector.Classifier
import com.example.graduationproject.data.model.DiseaseResultModel
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_disease_detector.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*


class DiseaseDetectorAvtivity : AppCompatActivity() {

    private lateinit var mClassifier: Classifier
    private lateinit var mBitmap: Bitmap
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null
    lateinit var locationManager: LocationManager
    private var hasGps = false

    companion object {
        var latitude: String = String()
        var longitude: String = String()
    }

    private val mCameraRequestCode = 0
    private val mCameraRequest = 1
    private val mGalleryRequestCode = 2

    private val mInputSize = 224
    private val mModelPath = "plant_disease_model_upgrade.tflite"
    private val mLabelPath = "plant_labels.txt"
    private val mSamplePath = "soybean.JPG"

    @SuppressLint("SourceLockedOrientationActivity")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disease_detector)

        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"

        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        progressBar_storage.visibility = View.GONE
        textView_percent.visibility = View.GONE

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLastLocation()

        getOrchardId()

        buttonEffect(button_input_tree)
        button_input_tree.setOnClickListener {
            showInputDialog()
        }

        buttonEffect(button_input_area)
        button_input_area.setOnClickListener {
            showInputDialog1()
        }

        btn_confirm_disease_activity.isEnabled = false
        mDetectButton.isEnabled = false


        buttonEffect(btn_confirm_disease_activity)
        btn_confirm_disease_activity.setOnClickListener {
            if (mResultTextView.text.isEmpty()) {
                Toast.makeText(this, "Bạn chưa nhận dạng!", Toast.LENGTH_LONG).show()
            } else if (tv_tree_disease_activity.text.isEmpty()) {
                Toast.makeText(this, "Bạn chưa nhập loại cây trồng!", Toast.LENGTH_LONG).show()
            } else if (tv_area.text.isEmpty()) {
                Toast.makeText(
                    this,
                    "Bạn chưa nhập diện tích bị vùng bị nhiễm!",
                    Toast.LENGTH_LONG
                ).show()
            } else if (mResultTextView.text == "null") {
                Toast.makeText(this, "Chưa nhận diện được loại sâu bệnh!", Toast.LENGTH_LONG)
                    .show()
            } else {
                if (!isOnline(this)) {
                    val snack = Snackbar.make(
                        it,
                        "Dữ liệu sẽ được lưu trữ sau khi bạn trực tuyến trở lại",
                        7000
                    )
                    snack.setAction("Kết nối internet", View.OnClickListener {
                        startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
                    })
                    snack.show()
                }
                //uploadImgToFirebaseStorage()

                saveDiagnosedDisease()
            }
        }

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        //Load model(plant_disease_model.tflite) and read the label file(plant_labels.txt)
        mClassifier = Classifier(assets, mModelPath, mLabelPath, mInputSize)

//        resources.assets.open(mSamplePath).use {
//            mBitmap = BitmapFactory.decodeStream(it)
//            mBitmap = Bitmap.createScaledBitmap(mBitmap, mInputSize, mInputSize, true)
//            mPhotoImageView.setImageBitmap(mBitmap)
//        }

        mPhotoImageView.setImageResource(R.drawable.img)

        //open camera
        buttonEffect(mCameraButton)
        mCameraButton.setOnClickListener {
            progressBar_storage.progress = 0
            mDetectButton.isEnabled = false
            mResultTextView.text = ""
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(callCameraIntent, mCameraRequestCode)
        }

        //open camera
        buttonEffect(button_take_infected_area_pic)
        button_take_infected_area_pic.setOnClickListener {

            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(callCameraIntent, mCameraRequest)
        }

        //open gallery
        buttonEffect(mGalleryButton)
        mGalleryButton.setOnClickListener {
            progressBar_storage.progress = 0
            mDetectButton.isEnabled = false
            mResultTextView.text = ""
            val callGalleryIntent = Intent(Intent.ACTION_PICK)
            callGalleryIntent.type = "image/*"
            startActivityForResult(callGalleryIntent, mGalleryRequestCode)
        }

        //detect disease button
        buttonEffect(mDetectButton)
        mDetectButton.setOnClickListener {
            //return the first element of the array, or null if arr is empty
            val results = mClassifier.recognizeImage(mBitmap).firstOrNull()
            mResultTextView.text = results?.title
            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Đang xử lý...")
            progressDialog.setMessage("Vui lòng chờ trong giây lát")
            progressDialog.show()
            val handler = Handler()
            handler.postDelayed({
                val intent = Intent(this, DiseaseDetails::class.java)
                progressDialog.cancel()
                if (mResultTextView.text.toString() == "null" || mResultTextView.text.toString().isEmpty()) {
                    Toast.makeText(this, "Chưa nhận dạng được loại sâu bệnh!", Toast.LENGTH_LONG)
                        .show()
                } else {
                    DiseaseDetails.diseaseName = mResultTextView.text.toString()
                    startActivity(intent)
                }
            }, 3000)
        }


        mResultTextView.setOnClickListener {
            val intent = Intent(this, DiseaseDetails::class.java)
            if (mResultTextView.text.toString() == "null" || mResultTextView.text.toString().isEmpty()) {
                Toast.makeText(this, "Chưa nhận dạng được loại sâu bệnh!", Toast.LENGTH_LONG)
                    .show()
            } else {
                DiseaseDetails.diseaseName = mResultTextView.text.toString()
                startActivity(intent)
            }

        }
    }

    override fun onResume() {
        super.onResume()
//        mResultTextView.text = ""
//        tv_tree_disease_activity.text = ""
//        progressBar_storage.progress = 0
//        btn_confirm_disease_activity.isEnabled = false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.itemId

        if (id == R.id.action_one) {
            //Toast.makeText(this, "Item One Clicked", Toast.LENGTH_LONG).show()
            return true
        }
        if (id == R.id.action_two) {
            val intent = Intent(this, DiseasesLibraryActivity::class.java)
            startActivity(intent)
            return true
        }
        if (id == R.id.action_three) {
            val intent = Intent(this, RealtimeCamXActivity::class.java)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)

    }


    var selectedPhotoUri: Uri? = null
    var downloadUri: String? = null
    var downloadUri1: String? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == mCameraRequestCode) {
            progressBar_storage.visibility = View.VISIBLE
            textView_percent.visibility = View.VISIBLE
            //if have img
            if (resultCode == Activity.RESULT_OK && data != null) {
                //random img's url id
                val filename = UUID.randomUUID().toString()
                // ref to firebase storage directory
                val ref = FirebaseStorage.getInstance().getReference("/leafs/$filename")
                //selectedPhotoUri = data.data
                mBitmap = data.extras!!.get("data") as Bitmap
                mBitmap = scaleImage(mBitmap)
                val toast = Toast.makeText(
                    this,
                    ("Ảnh được cắt thành: w= ${mBitmap.width} h= ${mBitmap.height}"),
                    Toast.LENGTH_LONG
                )
                toast.setGravity(Gravity.BOTTOM, 0, 20)
                toast.show()
                mPhotoImageView.setImageBitmap(mBitmap)
                //mResultTextView.text= "Your photo image set now."
                //upload taken img to firebase storage
                mPhotoImageView.isDrawingCacheEnabled = true
                mPhotoImageView.buildDrawingCache()
                val bitmap = (mPhotoImageView.drawable as BitmapDrawable).bitmap
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()
                var uploadTask = ref.putBytes(data)
                    .addOnSuccessListener {
                        ref.downloadUrl.addOnSuccessListener {
                            //get download URL
                            downloadUri = it.toString()
                            textView_percent.text = "100%"
                            //btn_confirm_disease_activity.isEnabled = true
                            mDetectButton.isEnabled = true
                        }
                    }
                    .addOnProgressListener {
                        val progress: Double = (100.0 * it.bytesTransferred) / it.totalByteCount
                        progressBar_storage.progress = progress.toInt()
                        textView_percent.text = "Đang tải ảnh:${progress.toInt()} %"
                    }
            } else {
                Toast.makeText(this, "Hủy chụp ảnh..", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == mGalleryRequestCode) {
            progressBar_storage.visibility = View.VISIBLE
            textView_percent.visibility = View.VISIBLE
            //if there is an img taken from gallery
            if (data != null) {
                val uri = data.data
                selectedPhotoUri = data.data
                try {
                    mBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                println("Success!!!")
                mBitmap = scaleImage(mBitmap)
                mPhotoImageView.setImageBitmap(mBitmap)

                //uploadImgToFirebaseStorage
                val filename = UUID.randomUUID().toString()
                val ref = FirebaseStorage.getInstance().getReference("/leafs/$filename")

                ref.putFile(selectedPhotoUri!!)
                    .addOnSuccessListener {
                        Log.d("Leafs", "Successfully uploaded: ${it.metadata?.path}")

                        ref.downloadUrl.addOnSuccessListener {
                            //get download URL
                            downloadUri = it.toString()
                        }
                        textView_percent.text = "100%"
                        //btn_confirm_disease_activity.isEnabled = true
                        mDetectButton.isEnabled = true
                    }
                    .addOnProgressListener {
                        val progress: Double = (100.0 * it.bytesTransferred) / it.totalByteCount
                        progressBar_storage.progress = progress.toInt()
                        textView_percent.text = "Đang tải ảnh:${progress.toInt()} %"
                    }
            }
        } else if (requestCode == mCameraRequest) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                //random img's url id
                val filename = UUID.randomUUID().toString()
                // ref to firebase storage directory
                val ref = FirebaseStorage.getInstance().getReference("/infectedArea/$filename")
                //selectedPhotoUri = data.data
                mBitmap = data.extras!!.get("data") as Bitmap
                mBitmap = scaleImage1(mBitmap)
                val toast = Toast.makeText(
                    this,
                    ("Ảnh được cắt thành: w= ${mBitmap.width} h= ${mBitmap.height}"),
                    Toast.LENGTH_LONG
                )
                toast.setGravity(Gravity.BOTTOM, 0, 20)
                toast.show()
                imageView_infectedArea.setImageBitmap(mBitmap)
                //mResultTextView.text= "Your photo image set now."
                //upload taken img to firebase storage
                imageView_infectedArea.isDrawingCacheEnabled = true
                imageView_infectedArea.buildDrawingCache()
                val bitmap = (imageView_infectedArea.drawable as BitmapDrawable).bitmap
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()
                var uploadTask = ref.putBytes(data)
                    .addOnSuccessListener {
                        ref.downloadUrl.addOnSuccessListener {
                            //get download URL
                            downloadUri1 = it.toString()

                            btn_confirm_disease_activity.isEnabled = true

                        }
                    }
            } else {
                Toast.makeText(this, "Hủy chụp ảnh..", Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(this, "Không nhận ra request code!", Toast.LENGTH_LONG).show()
        }
    }

    //resize img because model only expects input shape 224*224
    private fun scaleImage(bitmap: Bitmap?): Bitmap {
        val orignalWidth = bitmap!!.width
        val originalHeight = bitmap.height
        val scaleWidth = mInputSize.toFloat() / orignalWidth
        val scaleHeight = mInputSize.toFloat() / originalHeight
        val matrix = Matrix()
        matrix.postScale(scaleWidth, scaleHeight)
        return Bitmap.createBitmap(bitmap, 0, 0, orignalWidth, originalHeight, matrix, true)
    }

    private fun scaleImage1(bitmap: Bitmap?): Bitmap {
        val orignalWidth = bitmap!!.width
        val originalHeight = bitmap.height
        val scaleWidth = 1000.toFloat() / orignalWidth
        val scaleHeight = 1000.toFloat() / originalHeight
        val matrix = Matrix()
        matrix.postScale(scaleWidth, scaleHeight)
        return Bitmap.createBitmap(bitmap, 0, 0, orignalWidth, originalHeight, matrix, true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    //get orchard list and display it in spinner
    private fun getOrchardId() {
        val db = FirebaseFirestore.getInstance()
        db.collection("Orchard").get().addOnCompleteListener { task ->
            if (task.isSuccessful) {

                val list = arrayListOf<String>()

                val adapterArray = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)

                btn_input_orchard_disease_activity.adapter = adapterArray

                btn_input_orchard_disease_activity.onItemSelectedListener = object :

                    AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        tv_orchard_id_disease_activity.text = list[position]
                    }
                }
                for (document in task.result!!) {
                    list.add(document.id)
                }
                adapterArray.notifyDataSetChanged()
            }
        }
    }

    fun showInputDialog() {
        MaterialDialog(this)
            .show {
                input(
                    waitForPositiveButton = true,
                    allowEmpty = false
                ) { dialog, name ->
                    setNameToTextView(name.toString())
                }
                title(R.string.input_plant)
                positiveButton(R.string.btn_dialog_input_orchard)
            }

    }

    private fun setNameToTextView(name: String) {
        tv_tree_disease_activity.text = name
    }

    fun showInputDialog1() {
        MaterialDialog(this)
            .show {
                input(
                    waitForPositiveButton = true,
                    allowEmpty = false
                ) { dialog, name ->
                    setAreaToTextView(name.toString())
                }
                title(R.string.input_area)
                positiveButton(R.string.btn_dialog_input_orchard)
            }

    }

    private fun setAreaToTextView(area: String) {
        tv_area.text = area
    }

    fun showInputDialog2() {
        MaterialDialog(this)
            .show {
                input(
                    waitForPositiveButton = true,
                    allowEmpty = false
                ) { dialog, name ->
                    setResultToTextView(name.toString())
                }
                title(R.string.input_disease)
                positiveButton(R.string.btn_dialog_input_orchard)
            }

    }

    private fun setResultToTextView(area: String) {
        mResultTextView.text = area
    }

    //save diagnosed result to firebase
    private fun saveDiagnosedDisease() {
        val ref = FirebaseFirestore.getInstance()
        val model = DiseaseResultModel()
        val currentUser = FirebaseAuth.getInstance().currentUser
        model.plant = tv_tree_disease_activity.text.toString()
        model.diagnosedDisease = mResultTextView.text.toString()
        model.orchardId = tv_orchard_id_disease_activity.text.toString()
        model.imgUri = downloadUri.toString()
        model.uid = currentUser!!.uid
        model.infectedAreaImgUri = downloadUri1.toString()
        model.infectedArea = tv_area.text.toString()
        if(SplashScreenActivity.latitude.isEmpty() && SplashScreenActivity.longitude.isEmpty()){
            model.lat = latitude
            model.long = longitude
        }else{
            model.lat = SplashScreenActivity.latitude
            model.long = SplashScreenActivity.longitude
        }

        ref.collection("DiseasesDetectorResults").add(model)
            .addOnSuccessListener { documentReference ->
                try {
                    showDialogOK("Lưu kết quả thành công!",
                        DialogInterface.OnClickListener { _, which ->
                            when (which) {
                                DialogInterface.BUTTON_POSITIVE -> restoreOriginalState()
                            }
                        })
                } catch (e: Exception) {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }

            }
            .addOnFailureListener { e ->
                //Log.w("Firebase", "Error adding document", e)
                Toast.makeText(this, "Lưu thất bại!", Toast.LENGTH_LONG).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
    }

    private fun uploadImgToFirebaseStorage() {
        if (selectedPhotoUri == null) return
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/leafs/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                Log.d("Leafs", "Successfully uploaded: ${it.metadata?.path}")
                ref.downloadUrl.addOnSuccessListener {
                    Log.d("Leafs", "Successfully uploaded url")
                    downloadUri = it.toString()
                }
            }
    }

    private fun restoreOriginalState() {
//        val intent = Intent(this, MainActivity::class.java)
//        startActivity(intent)
        mResultTextView.text = ""
        tv_tree_disease_activity.text = ""
        tv_area.text = ""
        progressBar_storage.progress = 0
        progressBar_storage.visibility = View.GONE
        textView_percent.visibility = View.GONE
        btn_confirm_disease_activity.isEnabled = false
        mDetectButton.isEnabled = false
        imageView_infectedArea.setImageResource(R.drawable.img)
        mPhotoImageView.setImageResource(R.drawable.img)
    }

    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("Đồng ý", okListener)
            .create()
            .show()
    }

    private fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    private fun getLastLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (hasGps) {
            fusedLocationClient?.lastLocation!!.addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    lastLocation = task.result
                    longitude = (lastLocation)!!.longitude.toString()
                    latitude = (lastLocation)!!.latitude.toString()
                    Log.d("DiseaseLocation", "Latitude: $latitude")
                    Log.d("DiseaseLocation", "Longitude: $longitude")
                } else {
                    Log.w("Location", "getLastLocation:exception", task.exception)
                    //Toast.makeText(this, "Fail to get location", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
