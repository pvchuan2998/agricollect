package com.example.graduationproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.graduationproject.data.model.DiseaseDetail
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_diseases_library.*
import kotlinx.android.synthetic.main.disease_overall_info_row.view.*
import java.util.*

class DiseasesLibraryActivity : AppCompatActivity() {

    private lateinit var viewModel: FirestoreViewModel
    private val arrayList = ArrayList<DiseaseDetail>()
    private val displayList = ArrayList<DiseaseDetail>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diseases_library)
        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"
        actionBar.setDisplayHomeAsUpEnabled(true)

        val repository = FirestoreRepository()
        val factory = FirestoreViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(FirestoreViewModel::class.java)

        viewModel.fetchDiseaseData().observe(this, androidx.lifecycle.Observer { it ->
            arrayList.addAll(it)
            displayList.addAll(arrayList)
            val adapter = DiseaseLibAdapter(displayList, this)
            recyclerview_disease_library.layoutManager = LinearLayoutManager(this)
            recyclerview_disease_library.adapter = adapter
            pb_loading_disease_library.visibility = View.GONE
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu!!.findItem(R.id.search)

        if(menuItem != null){
            val searchView = menuItem.actionView as SearchView

            val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            editText.hint = "Tìm kiếm..."

            searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                override fun onQueryTextChange(newText: String?): Boolean {

                    if(newText!!.isNotEmpty()){
                        displayList.clear()
                        val search = newText.toLowerCase(Locale.getDefault())
                        arrayList.forEach{
                            if(it.name.toLowerCase(Locale.getDefault()).contains(search)){
                                displayList.add(it)
                            }
                        }

                        recyclerview_disease_library.adapter!!.notifyDataSetChanged()
                    }
                    else{
                        displayList.clear()
                        displayList.addAll(arrayList)
                        recyclerview_disease_library.adapter!!.notifyDataSetChanged()
                    }

                    return true
                }

                override fun onQueryTextSubmit(query: String?): Boolean {

                    return true
                }
            })
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }
}
