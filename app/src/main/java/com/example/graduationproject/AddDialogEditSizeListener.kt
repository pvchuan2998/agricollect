package com.example.graduationproject

interface AddDialogEditSizeListener {
    fun onOkButtonSizeClicked(width: String, height: String)
}