package com.example.graduationproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.hcmus.k16101353149.iotcore.connectivity.MQTTClient
import com.hcmus.k16101353149.iotcore.sensor.IoTSensor
import com.hcmus.k16101353149.iotcore.sensor.SensorManager
import kotlinx.android.synthetic.main.activity_api_processing.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CollectingSensorDataActivity : AppCompatActivity() {

    private val SPLASH_DELAY: Long = 3000 //5 seconds
    private var mDelayHandler: Handler? = null
    private var progressBarStatus = 0
    var dummy:Int = 0

    private val mSensorManagerListener = object : SensorManager.SensorDataChangedListener {
        override fun onSensorDataChanged(sensorData: HashMap<String, Float>) {
            Log.d("RegisterActivity", "onSensorDataChanged - START - sensorData=$sensorData")
            sensorData.forEach {
                val key = it.key
                val value = it.value
                when (key) {
                    IoTSensor.AIR_HUMID.name -> ShowEnvironmentInfoActivity.air_humid = value.toString()
                    IoTSensor.AIR_TEMP.name -> ShowEnvironmentInfoActivity.air_temp = value.toString()
                    IoTSensor.SOIL_HUMID.name -> ShowEnvironmentInfoActivity.soil_humid = value.toString()
                    IoTSensor.SOIL_TEMP.name -> ShowEnvironmentInfoActivity.soil_temp = value.toString()
                    IoTSensor.SOIL_PH.name -> ShowEnvironmentInfoActivity.ph = value.toString()
                    IoTSensor.SOIL_EC.name -> ShowEnvironmentInfoActivity.ec = value.toString()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_api_processing)
        supportActionBar?.hide()

        mDelayHandler = Handler()
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)

        AgriCollectApplication.startSensorListener(mSensorManagerListener)
        CoroutineScope(Dispatchers.IO).launch {
            MQTTClient.testSoilElectricalConductivitySensor()
        }
    }

    private fun launchMainActivity() {
        val intent = Intent(this, ShowEnvironmentInfoActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        this.finish()
        mDelayHandler!!.removeCallbacks(mRunnable)
    }

    private val mRunnable: Runnable = Runnable {

        Thread {
            while (progressBarStatus < 100) {
                // performing some dummy operation
                try {
                    dummy += 1
                    Thread.sleep(50)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                // tracking progress
                progressBarStatus = dummy

                // Updating the progress bar
                splash_screen_progress_bar.progress = progressBarStatus
            }

            //splash_screen_progress_bar.setProgress(10)
            launchMainActivity()

        }.start()
    }

    override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        AgriCollectApplication.stopSensorListener(mSensorManagerListener)

        super.onDestroy()
    }
}
