package com.example.graduationproject
import android.app.AlertDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.chintanpatel.materialeventcalendar.CalenderView
import com.chintanpatel.materialeventcalendar.EventItem
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.android.synthetic.main.activity_calendar.*
import kotlinx.android.synthetic.main.dialog_create_event_calendar.view.*
import kotlin.collections.ArrayList


class CalendarActivity : AppCompatActivity() {

    private lateinit var viewModel: FirestoreViewModel
    val currentUser = FirebaseAuth.getInstance().currentUser
    private val eventList: ArrayList<EventItem> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)
        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"

        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        val repository = FirestoreRepository()
        val factory = FirestoreViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(FirestoreViewModel::class.java)

        viewModel.fetchEvent().observe(this, androidx.lifecycle.Observer { it ->

            eventList.clear()
            it.forEach {
                eventList.add(EventItem(it.startDay, it.endDay, it.eventName))
                Log.e("CalendarActivity","Loaded document: $eventList")
            }
            eventCalendar.addEventList(eventList)
            pb_loading_calendar.visibility = View.GONE
        })

        //fetchCollectionHistory()

        eventCalendar.setCalenderEventClickListener(object : CalenderView.CalenderEventClickListener {
            override fun onEventClick(eventItem: EventItem) {
                showDialogOK(eventItem.title,
                    DialogInterface.OnClickListener { dialog, which ->
                        when(which){
                            DialogInterface.BUTTON_POSITIVE -> dialog.dismiss()
                        }
                    })
            }
        })


        fab.setOnClickListener {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_create_event_calendar, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Tạo sự kiện")

            val mAlertDialog = mBuilder.show()

            mDialogView.btn_ok.setOnClickListener {
                val start = mDialogView.et_dayStart.text.toString()
                val end = mDialogView.et_dayEnd.text.toString()
                val event = mDialogView.et_eventName.text.toString()

                if(!mDialogView.et_dayStart.text.isNullOrEmpty()
                    && !mDialogView.et_dayEnd.text.isNullOrEmpty()
                    && !mDialogView.et_eventName.text.isNullOrEmpty()){

                    val eventData = Event()
                    eventData.uid = currentUser!!.uid
                    eventData.startDay = start
                    eventData.endDay = end
                    eventData.eventName = event

                    viewModel.saveEvent(eventData, this)
                    mAlertDialog.dismiss()
                }
                else{
                    Toast.makeText(this, "Bạn cần nhập đầy đủ các vùng!", Toast.LENGTH_LONG).show()
                }

            }

            mDialogView.btn_cancel.setOnClickListener {
                mAlertDialog.dismiss()
            }

        }

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("Đồng ý", okListener)
            .create()
            .show()
    }

//    private fun saveEvent(start: String, end: String, event: String){
//        val ref = FirebaseFirestore.getInstance()
//        val currentUser = FirebaseAuth.getInstance().currentUser
//        val eventData = Event()
//        eventData.uid = currentUser!!.uid
//        eventData.startDay = start
//        eventData.endDay = end
//        eventData.eventName = event
//
//        ref.collection("Event").add(eventData)
//            .addOnSuccessListener { documentReference ->
//                //Log.d("Firebase", "DocumentSnapshot written with ID: ${documentReference.id}")
//                Toast.makeText(this, "Lưu lịch sử thành công!", Toast.LENGTH_LONG).show()
//            }
//            .addOnFailureListener { e ->
//                //Log.w("Firebase", "Error adding document", e)
//                Toast.makeText(this, "Lưu lịch sử thất bại!", Toast.LENGTH_LONG).show()
//                val intent = Intent(this, MainActivity::class.java)
//                startActivity(intent)
//
//            }
//    }

    private fun fetchCollectionHistory(){
        val db = FirebaseFirestore.getInstance()
        val currentUser = FirebaseAuth.getInstance().currentUser
        val eventList: ArrayList<EventItem> = arrayListOf()
        var registration: ListenerRegistration? = null
        if (currentUser != null) {
            db.collection("Event")
                .whereEqualTo("uid", currentUser.uid)
//                .get().addOnSuccessListener { result ->
//                    val list = result.toObjects(Event::class.java)
//                        list.forEach{
//                            Log.e("CalendarActivity","Loaded document: $it")
//                            eventList.add(EventItem(it.startDay, it.endDay, it.eventName))
//                            //eventCalendar.addEventList(eventList)
//                    }
//                    eventCalendar.addEventList(eventList)
//                }
                .addSnapshotListener{data, e ->
                    if (e != null) {
                        Log.w("TAG", "Listen failed.", e)
                        return@addSnapshotListener
                    }else{
                        if(data != null) {
                            val list = data.toObjects(Event::class.java)
                            eventList.clear()
                            list.forEach {
                                //Log.e("CalendarActivity","Loaded document: $it")
                                eventList.add(EventItem(it.startDay, it.endDay, it.eventName))
                                Log.e("CalendarActivity","Loaded document: $eventList")
                            }
                            eventCalendar.addEventList(eventList)
                        }
                    }
                    registration?.remove()
                }
        }

    }


}

data class Event(var startDay: String, var endDay: String, var eventName: String, var uid: String) {
    constructor():this("", "", "", "")
}
