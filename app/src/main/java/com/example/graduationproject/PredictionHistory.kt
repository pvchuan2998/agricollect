package com.example.graduationproject



import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.graduationproject.data.model.DiseaseResultModel
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_prediction_history.*
import kotlinx.android.synthetic.main.disease_detection_history_row.view.*

class PredictionHistory : AppCompatActivity() {

    private lateinit var viewModel: FirestoreViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prediction_history)
        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"

        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        val repository = FirestoreRepository()
        val factory = FirestoreViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(FirestoreViewModel::class.java)

        viewModel.fetchPredictionHistory().observe(this, androidx.lifecycle.Observer { it ->
            val adapter = GroupAdapter<GroupieViewHolder>()
            recyclerview_prediction_history.adapter = adapter
            adapter.setOnItemClickListener { item, view ->
                //val itemDisease = item as History
                val intent = Intent(view.context, DiseaseDetails::class.java)
                //intent.putExtra("Disease_Name", itemDisease.history)
                startActivity(intent)
                Log.d("disease","Clicked")
            }
            it.forEach {
                adapter.add(History(it))
                pb_loading_prediction.visibility = View.GONE
            }
        })
    }

//    private fun fetchCollectionHistory(){
//        Log.e("SwitchFarmActivity","fetchFarms function called")
//        val db = FirebaseFirestore.getInstance()
//        val currentUser = FirebaseAuth.getInstance().currentUser
//        val adapter = GroupAdapter<GroupieViewHolder>()
//        recyclerview_prediction_history.adapter = adapter
//        if (currentUser != null) {
//            db.collection("DiseasesDetectorResults")
//                .whereEqualTo("uid", currentUser.uid)
//                .get().addOnSuccessListener { result ->
//                    val historyList = result.toObjects(DiseaseResultModel::class.java)
//                    historyList.forEach{
//                        Log.e("DiseasesDetectorResults","Loaded document: $it")
//                        //adapter.notifyDataSetChanged()
//                        adapter.add(History(it))
//                        pb_loading_prediction.visibility = View.GONE
//                    }
//                }
//        }
//
//    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    class History(private val history: DiseaseResultModel): Item<GroupieViewHolder>() {
        override fun bind(viewHolder: GroupieViewHolder, position: Int) {

            val textTime: String = "Thời gian: ${history.time.toString()}"
            val textName: String = "Tên vườn: ${history.orchardId}"
            val disease: String = "Loại sâu bệnh: ${history.diagnosedDisease}"
            val plant: String = "Loại cây trồng: ${history.plant}"



            Picasso.get()
                .load(history.imgUri)
                .resize(448,448)
                .into(viewHolder.itemView.imv_disease_detection_row);

            viewHolder.itemView.tv_disease_detection_row_time.text = textTime

            viewHolder.itemView.tv_disease_detection_row_orchard.text = textName

            viewHolder.itemView.tv_disease_detection_row_disease.text = disease

            viewHolder.itemView.tv_disease_detection_row_plant.text = plant

            viewHolder.itemView.setOnClickListener {
                DiseaseDetails.diseaseName = history.diagnosedDisease
                val intent = Intent(viewHolder.itemView.context, DiseaseDetails::class.java)
                viewHolder.itemView.context.startActivity(intent)
                Log.d("disease", DiseaseDetails.diseaseName!!)
            }



        }



        override fun getLayout(): Int {
            return R.layout.disease_detection_history_row
        }
    }


}
