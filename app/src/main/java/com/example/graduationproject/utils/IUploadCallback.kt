package com.example.graduationproject.utils

interface IUploadCallback {
    fun onProgressUpdate(percent:Int)
    fun onError()
    fun onFinish()
}