package com.example.graduationproject.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.graduationproject.SplashScreenActivity

class NotificationUtils private constructor() {
    private object Singleton {
        val INSTANCE = NotificationUtils()
    }

    companion object {

        private val TAG = NotificationUtils::class.java.simpleName

        private const val CHANNEL_ID = "com.example.graduationproject.utils.notificationutils"
        private const val NOTIFICATION_ID = 1

        @JvmStatic
        val instance: NotificationUtils by lazy { Singleton.INSTANCE }
    }

    private fun createNotificationChannels(
        context: Context, channelId: String,
        channelName: String, vibration: Boolean = false,
        importance: Int = NotificationManager.IMPORTANCE_DEFAULT
    ): NotificationManager {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val currentChannel = notificationManager.getNotificationChannel(channelId)

            if (currentChannel == null) {
                val channel = NotificationChannel(channelId, channelName, importance)
                channel.setShowBadge(true)
                channel.enableVibration(vibration)
                channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                notificationManager.createNotificationChannel(channel)
            }
        }

        return notificationManager
    }

    fun sendNotification(context: Context, subText: String, content: String, resIconId: Int) {
        val channelName = "Cảnh báo"

        createNotificationChannels(
            context,
            CHANNEL_ID,
            channelName,
            true,
            NotificationManager.IMPORTANCE_DEFAULT
        )

        val intent = Intent(context, SplashScreenActivity::class.java)
        val contentIntent =
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(resIconId)
            .setContentTitle(subText)
            .setContentText(content)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setCategory(Notification.CATEGORY_REMINDER)
            .setColor(Color.RED)
            .setContentIntent(contentIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setAutoCancel(true)
            .build()

        try {
            NotificationManagerCompat.from(context).cancel(NOTIFICATION_ID)
            NotificationManagerCompat.from(context).notify(NOTIFICATION_ID, builder)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}