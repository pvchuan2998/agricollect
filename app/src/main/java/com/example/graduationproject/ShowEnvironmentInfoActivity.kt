package com.example.graduationproject

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.MaterialDialog
import com.example.graduationproject.data.model.CollectionHistory
import com.example.graduationproject.data.model.KetQuaDo
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_show_environment_info.*
import kotlinx.android.synthetic.main.dialog_edit_fruit_size.*
import kotlinx.android.synthetic.main.layout_img_review.*

class ShowEnvironmentInfoActivity : AppCompatActivity() {

//    val PERMISSION_CODE = 1000
//    var image_uri: Uri? = null
//    private val IMAGE_CAPTURE_CODE = 1001
//    var clicked: Boolean = false
    private lateinit var viewModel: FirestoreViewModel
    private var input_fruit_name_flag = false
    private var input_fruit_size_flag = false

    companion object {
        var air_humid: String? = null
        var soil_humid: String? = null
        var air_temp: String? = null
        var soil_temp: String? = null
        var ec: String? = null
        var ph: String? = null
        var width_input: String? = null
        var height_input: String? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_environment_info)

        getOrchardId()

        val repository = FirestoreRepository()
        val factory = FirestoreViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(FirestoreViewModel::class.java)
        val data = KetQuaDo()

        val actionBar = supportActionBar
        actionBar!!.title = "Quay về"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        textView_soil_temp.text = "$soil_temp (oC)"
        textView_air_temp.text = "$air_temp (oC)"
        textView_soil_humid.text = "$soil_humid (%)"
        textView_air_humid.text = "$air_humid (%)"
        textView_ec.text = "$ec (mS/cm)"
        textView_ph.text = ph


        cb_fruit_type_envi_info.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_UP ->{
                    if(event.getRawX() >= cb_fruit_type_envi_info.right - cb_fruit_type_envi_info.compoundDrawables[2].bounds.width()){
                        //Log.e("Tag", "Action Up")
                        EditFruitNameDialog(this,
                            object : AddDialogEditNameListener{
                                override fun onOKButtonClicked(text: String) {
                                        val isNumeric = text.matches("-?\\d+(\\.\\d+)?".toRegex())
                                        if(!isNumeric && text.isNotBlank()) {
                                            data.loaiTrai = text
                                            cb_fruit_type_envi_info.text = text
                                            input_fruit_name_flag = true
                                        }
                                        else{
                                            data.loaiTrai = ""
                                            cb_fruit_type_envi_info.text = ""
                                            cb_fruit_type_envi_info.isChecked = false
                                            Toast.makeText(this@ShowEnvironmentInfoActivity, "Loại trái phải là chữ! Vui lòng nhập lại!", Toast.LENGTH_LONG).show()
                                        }
                                    }
                            }).show()
                    }
                }

            }
            return@setOnTouchListener false
        }

        cb_size_envi_info.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_UP ->{
                    if(event.getRawX() >= cb_size_envi_info.right - cb_size_envi_info.compoundDrawables[2].bounds.width()){
                        Log.e("Tag", "Action Up")
                        EditFruitSizeDialog(this,
                            object: AddDialogEditSizeListener{
                                override fun onOkButtonSizeClicked(width: String, height: String) {
                                        val numericW = width.matches("-?\\d+(\\.\\d+)?".toRegex())
                                        val numericH = height.matches("-?\\d+(\\.\\d+)?".toRegex())
                                        if(numericW && numericH){
                                            width_input = width
                                            height_input = height
                                            textView_w_h.text = "$width x $height"
                                            input_fruit_size_flag = true
                                        }
                                        else{
                                            textView_w_h.text = "0 x 0"
                                            cb_size_envi_info.isChecked = false
                                            Toast.makeText(this@ShowEnvironmentInfoActivity, "Kích thước phải là một số! Vui lòng nhập lại!", Toast.LENGTH_LONG).show()
                                        }
                                }
                            }).show()
                    }
                }
            }
            return@setOnTouchListener false
        }


        buttonEffect(button_select_all_envi_info)
        button_select_all_envi_info.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                uncheckAllCheckbox()
            }
            else{
                checkAllCheckbox()
            }
        }

        buttonEffect(button_cancel_envi_info)
        button_cancel_envi_info.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        buttonEffect(button_confirm_envi_info)
        button_confirm_envi_info.setOnClickListener {
            data.longtitude = SplashScreenActivity.longitude
            data.latitude = SplashScreenActivity.latitude
            data.maSoKhuVuon = tv_orchard_id_show_envi_activity.text.toString()
            if(cb_size_envi_info.isChecked && input_fruit_size_flag){
                data.kichThuocTrai_Dai = width_input!!.toDouble()
                data.kichThuocTrai_Rong = height_input!!.toDouble()
            }
            if(cb_temperature_soil_envi_info.isChecked){
                data.nhietDoDat = soil_temp!!.toDouble()
            }
            if(cb_temperature_air_envi_info.isChecked){
                data.nhietDoKK = air_temp!!.toDouble()
            }
            if(cb_humidity_soil_envi_info.isChecked){
                data.doAmDat = soil_humid!!.toDouble()
            }
            if(cb_humidity_air_envi_info.isChecked){
                data.doAmKK = air_humid!!.toDouble()
            }
            if(cb_fruit_type_envi_info.isChecked && input_fruit_name_flag){
                data.loaiTrai = cb_fruit_type_envi_info.text.toString()
            }
            if(cb_ec_envi_info.isChecked){
                data.doECDat = ec!!.toDouble()
            }
            if(cb_ph_envi_info.isChecked){
                data.doPHDat = ph!!.toDouble()
            }
            val currentUser = FirebaseAuth.getInstance().currentUser
            data.uid = currentUser!!.uid

            if(isUncheckAllCheckbox()){
                if(!isOnline(this)){
                    val snack = Snackbar.make(it,"Dữ liệu sẽ được lưu trữ sau khi bạn trực tuyến trở lại", 7000)
                    snack.setAction("Kết nối internet", View.OnClickListener {
                        startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
                    })
                    snack.show()
                }
                saveHistory()
                viewModel.saveDataPackage(data, this)
            }
            else{
                Toast.makeText(this, "Bạn cần chọn ít nhất 1 thông số!", Toast.LENGTH_LONG).show()
            }

        }

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    private fun checkAllCheckbox(){
        cb_fruit_type_envi_info.isChecked = true
        cb_size_envi_info.isChecked = true
        cb_ph_envi_info.isChecked = true
        cb_ec_envi_info.isChecked = true
        cb_humidity_air_envi_info.isChecked = true
        cb_humidity_soil_envi_info.isChecked = true
        cb_temperature_air_envi_info.isChecked = true
        cb_temperature_soil_envi_info.isChecked = true
    }
    private fun uncheckAllCheckbox(){
        cb_fruit_type_envi_info.isChecked = false
        cb_size_envi_info.isChecked = false
        cb_ph_envi_info.isChecked = false
        cb_ec_envi_info.isChecked = false
        cb_humidity_air_envi_info.isChecked = false
        cb_humidity_soil_envi_info.isChecked = false
        cb_temperature_air_envi_info.isChecked = false
        cb_temperature_soil_envi_info.isChecked = false
    }
    private fun isUncheckAllCheckbox(): Boolean{
        if( !cb_fruit_type_envi_info.isChecked &&
            !cb_size_envi_info.isChecked &&
            !cb_ph_envi_info.isChecked &&
            !cb_ec_envi_info.isChecked &&
            !cb_humidity_air_envi_info.isChecked &&
            !cb_humidity_soil_envi_info.isChecked &&
            !cb_temperature_air_envi_info.isChecked &&
            !cb_temperature_soil_envi_info.isChecked){
                return false
        }
       return true
    }


    private fun getOrchardId() {
        val db = FirebaseFirestore.getInstance()
        db.collection("Orchard").get().addOnCompleteListener { task ->
            if(task.isSuccessful){

                val list = arrayListOf<String>()

                val adapterArray = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)

                btn_input_orchard_show_envi_activity.adapter = adapterArray

                btn_input_orchard_show_envi_activity.onItemSelectedListener = object :

                    AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        tv_orchard_id_show_envi_activity.text = list[position]
                    }
                }
                for(document in task.result!!){
                    list.add(document.id)
                }
                adapterArray.notifyDataSetChanged()
            }
        }
    }

    private fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }

    private fun saveHistory(){
        val ref = FirebaseFirestore.getInstance()
        val currentUser = FirebaseAuth.getInstance().currentUser
        val historyData = CollectionHistory()
        historyData.orchardName =  tv_orchard_id_show_envi_activity.text.toString()
        historyData.userId = currentUser!!.uid
        if(cb_temperature_air_envi_info.isChecked){
            historyData.air_temp = air_temp.toString()
        }
        if(cb_humidity_soil_envi_info.isChecked){
            historyData.soil_humid = soil_humid.toString()
        }
        if(cb_humidity_air_envi_info.isChecked){
            historyData.air_temp = air_temp.toString()
        }
        if(cb_temperature_soil_envi_info.isChecked){
            historyData.soil_temp = soil_temp.toString()
        }
        if(cb_ec_envi_info.isChecked){
            historyData.ec = ec.toString()
        }
        if(cb_ph_envi_info.isChecked){
            historyData.ph = ph.toString()
        }
//        historyData.air_temp = air_temp.toString()
//        historyData.soil_temp = soil_temp.toString()
//        historyData.air_humid = air_humid.toString()
//        historyData.soil_humid = soil_humid.toString()
//        historyData.ec = ec.toString()
//        historyData.ph = ph.toString()

        ref.collection("CollectionHistory").add(historyData)
            .addOnSuccessListener { documentReference ->
                Toast.makeText(this, "Lưu lịch sử thành công!", Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Fail to save!", Toast.LENGTH_LONG).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

}
