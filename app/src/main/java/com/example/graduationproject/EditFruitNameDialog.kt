package com.example.graduationproject

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import kotlinx.android.synthetic.main.dialog_edit_fruit_name.*

class EditFruitNameDialog(context: Context, var dialogListener: AddDialogEditNameListener): AppCompatDialog(context){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_edit_fruit_name)

        button_ok.setOnClickListener {
            val name = editText_fruit_name.text.toString()
            dialogListener.onOKButtonClicked(name)
            dismiss()
        }

        button_cancel.setOnClickListener {
            cancel()
        }

    }

}