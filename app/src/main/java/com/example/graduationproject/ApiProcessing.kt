package com.example.graduationproject

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.graduationproject.Retrofit.IUploadAPI
import com.example.graduationproject.Retrofit.RetrofitClient
import com.example.graduationproject.utils.Common
import com.example.graduationproject.utils.IUploadCallback
import com.example.graduationproject.utils.ProgressRequestBody
import com.google.firebase.firestore.FirebaseFirestore
import com.hcmus.k16101353149.iotcore.connectivity.MQTTClient
import com.hcmus.k16101353149.iotcore.sensor.IoTSensor
import com.hcmus.k16101353149.iotcore.sensor.SensorManager
import kotlinx.android.synthetic.main.activity_api_processing.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.InputStream
import java.net.URISyntaxException

class ApiProcessing : AppCompatActivity(), IUploadCallback {

    private val SPLASH_DELAY: Long = 5000 //4 seconds
    private var mDelayHandler: Handler? = null
    private var progressBarStatus = 0

    //var dummy:Int = 0
    var flag: Boolean = false

    lateinit var mService: IUploadAPI
    private var selectedUri: Uri? = image_uri

    companion object {
        var image_uri: Uri? = null
    }

    private val mSensorManagerListener = object : SensorManager.SensorDataChangedListener {
        override fun onSensorDataChanged(sensorData: HashMap<String, Float>) {
            Log.d("RegisterActivity", "onSensorDataChanged - START - sensorData=$sensorData")
            sensorData.forEach {
                val key = it.key
                val value = it.value
                when (key) {
                    IoTSensor.AIR_HUMID.name -> HaveFruitResult.air_humid = value.toString()
                    IoTSensor.AIR_TEMP.name -> HaveFruitResult.air_temp = value.toString()
                    IoTSensor.SOIL_HUMID.name -> HaveFruitResult.soil_humid = value.toString()
                    IoTSensor.SOIL_TEMP.name -> HaveFruitResult.soil_temp = value.toString()
                    IoTSensor.SOIL_PH.name -> HaveFruitResult.ph = value.toString()
                    IoTSensor.SOIL_EC.name -> HaveFruitResult.ec = value.toString()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_api_processing)
        supportActionBar?.hide()

//        mDelayHandler = Handler()
//        //Navigate with delay
//        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)


        AgriCollectApplication.startSensorListener(mSensorManagerListener)
        CoroutineScope(Dispatchers.IO).launch {
            val db = FirebaseFirestore.getInstance()
            db.collection("ApiLink").document("link")
                    .get()
                    .addOnSuccessListener { document ->
                        if (document != null) {
                            val link = document.getString("link").toString()
                            Log.d("Data", link)
                            CoroutineScope(Dispatchers.IO).launch {
                                mService = RetrofitClient(link).client.create(IUploadAPI::class.java)
                                withContext(Dispatchers.Main) {
                                    uploadImageFileToApiServer()
                                }
                            }
                        } else {
                            Log.d("Data", "No such document")
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.e("Data", "Get failed with ", exception)
                    }

            MQTTClient.testSoilElectricalConductivitySensor()
        }
    }

    private fun launchMainActivity() {
        val intent = Intent(this, HaveFruitResult::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        this.finish()
        mDelayHandler!!.removeCallbacks(mRunnable)
    }

    private val mRunnable: Runnable = Runnable {

        Thread(Runnable {
//            while (progressBarStatus < 100) {
//                // performing some dummy operation
//                try {
//                    dummy += 1
//                    Thread.sleep(250)
//                } catch (e: InterruptedException) {
//                    e.printStackTrace()
//                }
//                // tracking progress
//                progressBarStatus = dummy
//
//                // Updating the progress bar
//                splash_screen_progress_bar.progress = progressBarStatus
//            }

            launchMainActivity()

        }).start()
    }


    override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        AgriCollectApplication.stopSensorListener(mSensorManagerListener)

        super.onDestroy()
    }

    override fun onProgressUpdate(percent: Int) {
        progressBarStatus = percent
        splash_screen_progress_bar.progress = progressBarStatus

    }

    override fun onError() {

    }

    override fun onFinish() {
        Log.e("onFinish", "Running")
        splash_screen_progress_bar.progress = 100
        mDelayHandler = Handler()
        //Navigate with delay
//        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
        //launchMainActivity()
    }


    private fun uploadImageFileToApiServer() {
        Log.e("api", "start")
        var file: File? = null
        //var file1: File?= null
        try {
            file = File(Common.getFilePath(this, selectedUri!!))
            Log.e("FilePath", file.toString())
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        //compress image to 1mb max
        val inputStream = BitmapUtils.compressedImageFile(Common.getFilePath(this, selectedUri!!).toString())
        if (inputStream != null) {
            Log.e("copyInputStreamToFile", "Running")
            file?.copyInputStreamToFile(inputStream)
        }

        if (file != null) {
            val requestBody = ProgressRequestBody(file, this)

            val body = MultipartBody.Part.createFormData("image", file.name, requestBody)
            Thread(Runnable {
                mService.uploadFile(body)
                        .enqueue(object : Callback<String> {
                            override fun onFailure(call: Call<String>, t: Throwable) {
                                Toast.makeText(this@ApiProcessing, t.message, Toast.LENGTH_LONG).show()
                                Log.e("api", t.message!!)
                                moveToMainActivity()
                            }

                            override fun onResponse(call: Call<String>, response: Response<String>) {
                                val stringResponse = response.body()?.toString()

                                try {
                                    val jsonObject = JSONObject(stringResponse)
                                    val label = jsonObject.getString("label")
                                    val width = jsonObject.getString("width")
                                    val height = jsonObject.getString("height")
                                    HaveFruitResult.label = label
                                    HaveFruitResult.width = width
                                    HaveFruitResult.height = height
                                    Log.e("apidata_label", label)
                                    Log.e("apidata_width", width)
                                    Log.e("apidata_height", height)
                                    Log.e("validation", jsonObject.getString("validation"))
                                    flag = true
                                    moveToMainActivity()
                                } catch (e: NullPointerException) {
                                    Toast.makeText(this@ApiProcessing, e.message, Toast.LENGTH_LONG).show()
                                    //moveToMainActivity()
                                    val handler = Handler()
                                    handler.postDelayed({
                                        moveToMainActivity()
                                    }, 4000)
                                }
                            }
                        })

            }).start()
        } else {
            Toast.makeText(this@ApiProcessing, "Fail!!", Toast.LENGTH_LONG).show()
        }
        Log.e("api", "end")


    }

    private fun moveToMainActivity() {
        val intent = Intent(this, HaveFruitResult::class.java)
        startActivity(intent)
    }


    object BitmapUtils {
        const val ONE_KO = 1024
        const val ONE_MO = ONE_KO * ONE_KO

        /**
         * Compress, if needed, an image file to be lower than or equal to 1 Mo
         *
         * @param filePath Image file path
         *
         * @return Stream containing data of the compressed image. Can be null
         */
        fun compressedImageFile(filePath: String): InputStream? {
            var quality = 100
            var inputStream: InputStream? = null
            if (filePath.isNotEmpty()) {
                var bufferSize = Integer.MAX_VALUE
                val byteArrayOutputStream = ByteArrayOutputStream()
                try {
                    val bitmap = BitmapFactory.decodeFile(filePath)
                    do {
                        if (bitmap != null) {
                            byteArrayOutputStream.reset()
                            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream)
                            bufferSize = byteArrayOutputStream.size()
                            //logD { "quality: $quality -> length: $bufferSize" }
                            Log.e("quality", bufferSize.toString())
                            quality -= 10
                        }
                    } while (bufferSize > ONE_MO)
                    inputStream = ByteArrayInputStream(byteArrayOutputStream.toByteArray())
                    byteArrayOutputStream.close()
                } catch (e: Exception) {
                    Log.e("api", e.message)
                }
            }
            return inputStream
        }
    }

    fun File.copyInputStreamToFile(inputStream: InputStream) {
        Log.e("copyInputStreamToFile", "Running")
        this.outputStream().use { fileOut ->
            inputStream.copyTo(fileOut)
        }
    }

}
