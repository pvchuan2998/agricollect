package com.example.graduationproject

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.graduationproject.data.model.OrchardModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_orchard_row.view.*

class ListOrchardAdapter(val arrayList: ArrayList<OrchardModel>, val context: Context) :
    RecyclerView.Adapter<ListOrchardAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindItems(model: OrchardModel){
            itemView.tv_orchard_name.text = model.name

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.list_orchard_row, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(arrayList[position])

        holder.itemView.setOnClickListener {
            val model = arrayList[position]
            OrchardDetailActivity.orchardId = model.id
            OrchardDetailActivity.ownerId = model.owner
            val intent = Intent(context, OrchardDetailActivity::class.java)
            context.startActivity(intent)
        }
    }
}