package com.example.graduationproject

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.graduationproject.data.model.DiseaseDetail
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.disease_overall_info_row.view.*

class DiseaseLibAdapter(val arrayList: ArrayList<DiseaseDetail>, val context: Context) :
    RecyclerView.Adapter<DiseaseLibAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindItems(model: DiseaseDetail){
            itemView.tv_disease_name_lib.text = model.name
            itemView.tv_disease_vnname_lib.text = model.vnName
            Picasso.get()
                .load(model.imageUrl)
                .into(itemView.imv_disease_lib);
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.disease_overall_info_row, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(arrayList[position])

        holder.itemView.setOnClickListener {
            val model = arrayList[position]
            DiseaseDetails.diseaseName = model.label
            val intent = Intent(context, DiseaseDetails::class.java)
            context.startActivity(intent)
        }
    }
}