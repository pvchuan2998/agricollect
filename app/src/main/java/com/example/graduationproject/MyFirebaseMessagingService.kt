package com.example.graduationproject

import android.R
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.graduationproject.utils.NotificationUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService: FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.e("token", "New token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        var notificationTitle: String? = null
        var notificationBody: String? = null

        // Check if message contains a notification payload
        // Check if message contains a notification payload
        if (remoteMessage.getNotification() != null) {
            Log.d(
                "TAG",
                "Message Notification Body: " + remoteMessage.notification!!.body
            )
            notificationTitle = remoteMessage.notification!!.title
            notificationBody = remoteMessage.notification!!.body
        }

        // If you want to fire a local notification (that notification on the top of the phone screen)
        // you should fire it from here
        // If you want to fire a local notification (that notification on the top of the phone screen)
        // you should fire it from here
        if (notificationBody != null && notificationTitle!= null) {
            NotificationUtils.instance.sendNotification(this, notificationTitle, notificationBody, R.drawable.ic_dialog_alert)
        }
    }
}