package com.example.graduationproject

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter (fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager){
    private val COUNT = 3

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when(position){
            0 -> fragment =  FragmentTakePhoto()
            1 -> fragment = FragmentHome()
            2 -> fragment = FragmentSettings()
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return COUNT
    }
}