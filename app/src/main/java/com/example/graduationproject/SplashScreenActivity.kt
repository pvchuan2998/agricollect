package com.example.graduationproject

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.content.ContextCompat
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.getSystemService
import com.example.graduationproject.ui.auth.LoginActivity
import com.example.graduationproject.ui.auth.RegisterActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*


class SplashScreenActivity : AppCompatActivity() {

    private val TAG = "tag"
    lateinit var locationManager: LocationManager
    private var hasGps = false

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null
    private val REQUEST_CHECK_SETTINGS = 0x1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.slash_screen_activity)
        supportActionBar?.hide()

        val connectionManager: ConnectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork : NetworkInfo? = connectionManager.activeNetworkInfo
        val isConnected : Boolean = activeNetwork?.isConnectedOrConnecting == true

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if(isConnected){

            //If check and request permissions successfully --> delay a few seconds and turn to login activity
            if(checkAndRequestPermissions()){
                //getLastLocation()
                val handler = Handler()
                handler.postDelayed({
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    // finish flash screen
                    finish()
                }, returnTime().toLong())
            }
        }
        else{
            //lost connection activity
            val intent = Intent(this, LostConnectionActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        getLastLocation()
        //enablegps()
    }

    companion object {
        val REQUEST_ID_MULTIPLE_PERMISSIONS = 1
        var SPLASH_TIME_OUT = 10000
        var latitude: String = String()
        var longitude: String = String()
    }

    private fun returnTime(): Int{
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if(hasGps){
            com.example.graduationproject.SplashScreenActivity.Companion.SPLASH_TIME_OUT = 7000
        }
        else{
            com.example.graduationproject.SplashScreenActivity.Companion.SPLASH_TIME_OUT = 15000
        }
        return com.example.graduationproject.SplashScreenActivity.Companion.SPLASH_TIME_OUT
    }

    private fun getLastLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if(hasGps) {
            fusedLocationClient?.lastLocation!!.addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    lastLocation = task.result
                    longitude = (lastLocation)!!.longitude.toString()
                    latitude = (lastLocation)!!.latitude.toString()
                    Log.d("Location1", "Network Latitude: $latitude")
                    Log.d("Location2", "Network Longitude: $longitude")
                } else {
                    Log.w(TAG, "getLastLocation:exception1", task.exception)
                    //Toast.makeText(this, "Fail to get location", Toast.LENGTH_LONG).show()
                }
            }
        }else{
//            showDialogOK("Bạn có muốn bật GPS?",
//                DialogInterface.OnClickListener { dialog , which ->
//                    when (which) {
//                        DialogInterface.BUTTON_POSITIVE -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
//                        DialogInterface.BUTTON_NEGATIVE -> dialog.dismiss()
//                    }
//                })
//            showDialogOK("Bạn có muốn bật GPS?",
//                DialogInterface.OnClickListener { dialog , which ->
//                    when (which) {
//                        DialogInterface.BUTTON_POSITIVE -> enablegps()
//                        DialogInterface.BUTTON_NEGATIVE -> dialog.dismiss()
//                    }
//                })
            enablegps()
        }
    }

    private fun checkAndRequestPermissions(): Boolean {
        val camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val internetPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)

        val listPermissionsNeeded = ArrayList<String>()

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if(internetPermission != PackageManager.PERMISSION_GRANTED){
            listPermissionsNeeded.add(Manifest.permission.INTERNET)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        Log.d(TAG, "Permission callback called")
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.INTERNET] = PackageManager.PERMISSION_GRANTED
                // Fill with actual results from user
                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                        &&  perms[Manifest.permission.INTERNET] == PackageManager.PERMISSION_GRANTED)
                    {
                        Log.d(TAG, "sms & location services permission granted")
                        // process the normal flow
                        //getLastLocation()
                        //enablegps()
                        val handler = Handler()
                        handler.postDelayed({
                            val intent = Intent(this, LoginActivity::class.java)
                            startActivity(intent)
                            // finish flash screen
                            finish()
                        }, 7000)
//                        val i = Intent(this, LoginActivity::class.java)
//                        startActivity(i)
//                        finish()
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ")
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        //                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET))
                        {
                            showDialogOK("Service Permissions are required for this app",
                                DialogInterface.OnClickListener { _, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestPermissions()
                                        DialogInterface.BUTTON_NEGATIVE ->
                                            // proceed with logic by disabling the related features or quit the app.(not done)
                                            finish()
                                    }
                                })
                        } else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?")
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }//permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                    }
                }
            }
        }

    }

    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("Đồng ý", okListener)
            .setNegativeButton("Hủy", okListener)
            .create()
            .show()
    }

    private fun explain(msg: String) {
        val dialog = AlertDialog.Builder(this)
        dialog.setMessage(msg)
            .setPositiveButton("Yes") { paramDialogInterface, paramInt ->
                //permissionsclass.requestPermission(type,code);
                startActivity(Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("com.example.graduationproject")))
            }
            .setNegativeButton("Cancel") { paramDialogInterface, paramInt -> finish() }
        dialog.show()
    }

    fun enablegps() {

        val mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(2000)
            .setFastestInterval(1000)

        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        settingsBuilder.setAlwaysShow(true)

        val result = LocationServices.getSettingsClient(this).checkLocationSettings(settingsBuilder.build())
        result.addOnCompleteListener { task ->

            //getting the status code from exception
            try {
                task.getResult(ApiException::class.java)
            } catch (ex: ApiException) {

                when (ex.statusCode) {

                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {

                        Toast.makeText(this,"GPS IS OFF",Toast.LENGTH_SHORT).show()

                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        val resolvableApiException = ex as ResolvableApiException

                        resolvableApiException.startResolutionForResult(this, REQUEST_CHECK_SETTINGS
                        )
                    } catch (e: IntentSender.SendIntentException) {
                        Toast.makeText(this,"PendingIntent unable to execute request.",Toast.LENGTH_SHORT).show()

                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        Toast.makeText(
                            this,
                            "Something is wrong in your GPS",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CHECK_SETTINGS){
            if(resultCode == Activity.RESULT_OK){
                val handler = Handler()
                handler.postDelayed({
                    locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

                    if(hasGps) {
                        fusedLocationClient?.lastLocation!!.addOnCompleteListener(this) { task ->
                            if (task.isSuccessful && task.result != null) {
                                lastLocation = task.result
                                longitude = (lastLocation)!!.longitude.toString()
                                latitude = (lastLocation)!!.latitude.toString()
                                Log.d("Location", "Network Latitude: $latitude")
                                Log.d("Location", "Network Longitude: $longitude")
                            } else {
                                Log.w(TAG, "getLastLocation:exception", task.exception)
                                //Toast.makeText(this, "Fail to get location", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }, 10000)
            }
        }
    }

}
