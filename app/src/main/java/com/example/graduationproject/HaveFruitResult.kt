package com.example.graduationproject

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.datePicker
import com.afollestad.materialdialogs.input.input
import com.example.graduationproject.data.model.CollectionHistory
import com.example.graduationproject.data.model.KetQuaDo
import com.example.graduationproject.data.repositories.FirestoreRepository
import com.example.graduationproject.ui.collectingresults.FirestoreViewModel
import com.example.graduationproject.ui.collectingresults.FirestoreViewModelFactory
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.takephoto_havefruit_result.*
import java.io.*
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS
import kotlin.collections.ArrayList

class HaveFruitResult : AppCompatActivity() {

    companion object {
        var image_uri: Uri? = null
        var label: String? = null
        var width: String? = null
        var height: String? = null
        var air_humid: String? = null
        var soil_humid: String? = null
        var air_temp: String? = null
        var soil_temp: String? = null
        var width_input: String? = null
        var height_input: String? = null
        var ph: String? = null
        var ec: String? = null
    }

    private lateinit var viewModel: FirestoreViewModel

    private var input_fruit_size_flag = false
    private var input_fruit_name_flag = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.takephoto_havefruit_result)
        supportActionBar?.hide()


        val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, image_uri)
        val resizedBitmap = resizeBitmap(bitmap, 1200, 800)
        imageview_fruit_envi_info.setImageBitmap(resizedBitmap)

        getOrchardId()

        val repository = FirestoreRepository()
        val factory = FirestoreViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(FirestoreViewModel::class.java)

        val sampleData = KetQuaDo()
        cb_fruit_type_envi_info.text = label
        textView17.text = "$width x $height"
        textView16.text = "$air_temp (oC)"
        textView20.text = "$air_humid (%)"
        textView14.text = "$soil_temp (oC)"
        textView15.text = "$soil_humid (%)"
        textView18.text = "$ec (mS/cm)"
        textView19.text = ph

        buttonEffect(btn_input_orchard)

        cb_fruit_type_envi_info.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    if (event.getRawX() >= cb_fruit_type_envi_info.right - cb_fruit_type_envi_info.compoundDrawables[2].bounds.width()) {
                        EditFruitNameDialog(this,
                                object : AddDialogEditNameListener {
                                    override fun onOKButtonClicked(text: String) {
                                        val isNumeric = text.matches("-?\\d+(\\.\\d+)?".toRegex())
                                        if(!isNumeric && text.isNotBlank()) {
                                            cb_fruit_type_envi_info.text = text
                                            input_fruit_name_flag = true
                                        }
                                        else{
                                            cb_fruit_type_envi_info.isChecked = false
                                            Toast.makeText(this@HaveFruitResult, "Loại trái phải là chữ! Vui lòng nhập lại!", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                }).show()
                    }
                }
            }
            return@setOnTouchListener false
        }

        cb_size_envi_info.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    if (event.getRawX() >= cb_size_envi_info.right - cb_size_envi_info.compoundDrawables[2].bounds.width()) {
                        EditFruitSizeDialog(this,
                                object : AddDialogEditSizeListener {
                                    override fun onOkButtonSizeClicked(width: String, height: String) {
                                        if(checkIsNumeric(width) && checkIsNumeric(height)){
                                            textView17.text = "$width x $height"
                                            width_input = width
                                            height_input = height
                                            input_fruit_size_flag = true
                                        }
                                        else{
                                            cb_size_envi_info.isChecked = false
                                            textView17.text = "0 x 0"
                                            width_input = ""
                                            height_input = ""
                                            Toast.makeText(this@HaveFruitResult, "Kích thước phải là một số! Vui lòng nhập lại!", Toast.LENGTH_LONG).show()
                                        }

                                    }
                                }).show()
                    }
                }
            }
            return@setOnTouchListener false
        }

        buttonEffect(button_confirm_envi_info)
        button_confirm_envi_info.setOnClickListener {

            if (cb_fruit_type_envi_info.isChecked && input_fruit_name_flag) {
                sampleData.loaiTrai = cb_fruit_type_envi_info.text.toString()
            } else if(cb_fruit_type_envi_info.isChecked) {
                sampleData.loaiTrai = label.toString()
            }
            else {
                sampleData.loaiTrai = ""
            }
            if(cb_size_envi_info.isChecked && input_fruit_size_flag){
                sampleData.kichThuocTrai_Dai = width_input!!.toDouble()
                sampleData.kichThuocTrai_Rong = height_input!!.toDouble()
            }
            else if(cb_size_envi_info.isChecked) {
                sampleData.kichThuocTrai_Dai = width!!.toDouble()
                sampleData.kichThuocTrai_Rong = height!!.toDouble()
            }
            else{
                sampleData.kichThuocTrai_Dai = 0.0
                sampleData.kichThuocTrai_Rong = 0.0
            }
            sampleData.longtitude = SplashScreenActivity.longitude
            Log.e("Long", SplashScreenActivity.longitude)
            Log.e("Lat", SplashScreenActivity.latitude)
            sampleData.latitude = SplashScreenActivity.latitude
            if(cb_temperature_air_envi_info.isChecked){
                sampleData.nhietDoKK = air_temp!!.toDouble()
            }
            if(cb_humidity_soil_envi_info.isChecked){
                sampleData.doAmDat = soil_humid!!.toDouble()
            }
            if(cb_humidity_air_envi_info.isChecked){
                sampleData.doAmKK = air_humid!!.toDouble()
            }
            if(cb_temperature_soil_envi_info.isChecked){
                sampleData.nhietDoDat = soil_temp!!.toDouble()
            }
            if(cb_ec_envi_info.isChecked){
                sampleData.doECDat = ec!!.toDouble()
            }
            if(cb_ph_envi_info.isChecked){
                sampleData.doPHDat = ph!!.toDouble()
            }

            sampleData.maSoKhuVuon = tv_orchard_id.text.toString()

            if(isUncheckAllCheckbox()){
                if(!isOnline(this)){
                    val snack = Snackbar.make(it,"Dữ liệu sẽ được lưu trữ sau khi bạn trực tuyến trở lại", 7000)
                    snack.setAction("Kết nối internet", View.OnClickListener {
                        startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
                    })
                    snack.show()
                }
                saveHistory()
                viewModel.saveDataPackage(sampleData, this)
            }
            else{
                Toast.makeText(this, "Bạn cần chọn ít nhất 1 thông số!", Toast.LENGTH_LONG).show()
            }

        }

        buttonEffect(button_select_all_have_fruit)
        button_select_all_have_fruit.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                uncheckAllCheckbox()
            } else {
                checkAllCheckbox()
            }
        }

        buttonEffect(button_cancel_envi_info)
        button_cancel_envi_info.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        //request permission
        Dexter.withActivity(this)
                .withPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    }
                    override fun onPermissionRationaleShouldBeShown(
                            permission: PermissionRequest?,
                            token: PermissionToken?
                    ) {
                    }
                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        Toast.makeText(this@HaveFruitResult, "You must accept permission", Toast.LENGTH_LONG).show()
                    }

                }).check()
    }

    private fun checkIsNumeric(str: String): Boolean {
        return str.matches("-?\\d+(\\.\\d+)?".toRegex())
    }

    private fun getOrchardId() {
        val db = FirebaseFirestore.getInstance()
        db.collection("Orchard").get().addOnCompleteListener { task ->
            if(task.isSuccessful){

                val list = arrayListOf<String>()

                val adapterArray = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)

                btn_input_orchard.adapter = adapterArray

                btn_input_orchard.onItemSelectedListener = object :

                    AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        tv_orchard_id.text = list[position]
                    }
                }
                for(document in task.result!!){
                    list.add(document.id)
                }
                adapterArray.notifyDataSetChanged()
            }
        }
    }


    private fun checkAllCheckbox() {
        cb_fruit_type_envi_info.isChecked = true
        cb_ec_envi_info.isChecked = true
        cb_humidity_air_envi_info.isChecked = true
        cb_humidity_soil_envi_info.isChecked = true
        cb_temperature_air_envi_info.isChecked = true
        cb_temperature_soil_envi_info.isChecked = true
        cb_ph_envi_info.isChecked = true
        cb_size_envi_info.isChecked = true
    }

    private fun uncheckAllCheckbox() {
        cb_fruit_type_envi_info.isChecked = false
        cb_ec_envi_info.isChecked = false
        cb_humidity_air_envi_info.isChecked = false
        cb_humidity_soil_envi_info.isChecked = false
        cb_temperature_air_envi_info.isChecked = false
        cb_temperature_soil_envi_info.isChecked = false
        cb_ph_envi_info.isChecked = false
        cb_size_envi_info.isChecked = false
    }

    private fun isUncheckAllCheckbox(): Boolean {
        if( !cb_fruit_type_envi_info.isChecked &&
            !cb_ec_envi_info.isChecked &&
            !cb_humidity_air_envi_info.isChecked &&
            !cb_humidity_soil_envi_info.isChecked &&
            !cb_temperature_air_envi_info.isChecked &&
            !cb_temperature_soil_envi_info.isChecked &&
            !cb_ph_envi_info.isChecked &&
            !cb_size_envi_info.isChecked ){
                return false
        }
       return true
    }

//    private fun compressBitmap(bitmap: Bitmap, quality: Int): Bitmap {
//        // Initialize a new ByteArrayStream
//        val stream = ByteArrayOutputStream()
//
//        // Compress the bitmap with JPEG format and quality 50%
//        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream)
//
//        val byteArray = stream.toByteArray()
//
//        // Finally, return the compressed bitmap
//        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
//    }

    private fun resizeBitmap(bitmap: Bitmap, width: Int, height: Int): Bitmap {
        return Bitmap.createScaledBitmap(bitmap, width, height, false)
    }

    private fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }

    private fun saveHistory(){
        val ref = FirebaseFirestore.getInstance()
        val currentUser = FirebaseAuth.getInstance().currentUser
        val historyData = CollectionHistory()
        historyData.orchardName =  tv_orchard_id.text.toString()
        historyData.userId = currentUser!!.uid
        if(cb_temperature_air_envi_info.isChecked){
            historyData.air_temp = air_temp.toString()
        }
        if(cb_humidity_soil_envi_info.isChecked){
            historyData.soil_humid = soil_humid.toString()
        }
        if(cb_humidity_air_envi_info.isChecked){
            historyData.air_temp = air_temp.toString()
        }
        if(cb_temperature_soil_envi_info.isChecked){
            historyData.soil_temp = soil_temp.toString()
        }
        if(cb_ec_envi_info.isChecked){
            historyData.ec = ec.toString()
        }
        if(cb_ph_envi_info.isChecked){
            historyData.ph = ph.toString()
        }
//        historyData.air_temp = air_temp.toString()
//        historyData.soil_temp = soil_temp.toString()
//        historyData.air_humid = air_humid.toString()
//        historyData.soil_humid = soil_humid.toString()
//        historyData.ec = ec.toString()
//        historyData.ph = ph.toString()

        ref.collection("CollectionHistory").add(historyData)
            .addOnSuccessListener { documentReference ->
                //Log.d("Firebase", "DocumentSnapshot written with ID: ${documentReference.id}")
                Toast.makeText(this, "Lưu lịch sử thành công!", Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { e ->
                //Log.w("Firebase", "Error adding document", e)
                Toast.makeText(this, "Lưu lịch sử thất bại!", Toast.LENGTH_LONG).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    override fun onDestroy() {
//        image_uri = null
//        label = null
//        width = null
//        height = null
//        air_humid = null
//        soil_humid = null
//        air_temp = null
//        soil_temp = null
//        width_input = null
//        height_input = null
//        ph = null
//        ec = null
        super.onDestroy()
    }
}