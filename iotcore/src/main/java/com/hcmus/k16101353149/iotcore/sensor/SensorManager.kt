package com.hcmus.k16101353149.iotcore.sensor

import android.util.Log
import com.hcmus.k16101353149.iotcore.Constants
import com.hcmus.k16101353149.iotcore.connectivity.MQTTBroker
import com.hcmus.k16101353149.iotcore.connectivity.MQTTBrokerListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.HashMap

class SensorManager(listSupportedSensor: List<IoTSensor>) : Observable() {

    companion object {
        private val TAG = SensorManager::class.java.simpleName
    }

    enum class ConnectionStatus {
        NONE,
        CONNECT,
        DISCONNECT,
        CONNECTION_LOST;

        companion object {
            fun getStatusStr(status: ConnectionStatus): String {
                return when (status) {
                    CONNECT -> "Đã Kết Nối"
                    DISCONNECT,
                    CONNECTION_LOST -> "Mất Kết Nối"
                    NONE -> ""
                }
            }
        }
    }

    private val mSensorComposite: SensorComposite = SensorComposite(listSupportedSensor)
    private val mListenerList: ArrayList<SensorListener> = arrayListOf()
    val mIotKitConnectionList: LinkedHashMap<String, ConnectionStatus> = linkedMapOf()

    private val broker = MQTTBroker()
    private val mMQTTBrokerListener = MQTTBrokerListener(object : MQTTBrokerListener.HandlerListener {
        override fun onReceived(clientId: String, username: String, topic: String, content: String) {
            CoroutineScope(Dispatchers.IO).launch {
                Log.d(TAG, "HandlerListener - onReceived - START")
                val hashMap = HashMap<String, Float>()
                when (topic) {
                    Constants.AIR_SENSOR -> {
                        Log.d(TAG, "HandlerListener - onReceived - AIR_SENSOR")
                        content.split(",").forEach {
                            it.split("=").let { mapValue ->
                                val type = mapValue[0]
                                val value = mapValue[1].toFloat()
                                when (type) {
                                    Constants.TEMP -> {
                                        Log.d(TAG, "HandlerListener - onReceived - TEMP")
                                        hashMap.putAll(
                                                mSensorComposite.updateSensorData(IoTSensor.AIR_TEMP.name, value)
                                        )
                                    }
                                    Constants.HUMID -> {
                                        Log.d(TAG, "HandlerListener - onReceived - HUMID")
                                        hashMap.putAll(
                                                mSensorComposite.updateSensorData(IoTSensor.AIR_HUMID.name, value)
                                        )
                                    }
                                }
                            }
                        }
                    }
                    Constants.SOIL_SENSOR -> {
                        Log.d(TAG, "HandlerListener - onReceived - SOIL_SENSOR")
                        content.split(",").forEach {
                            it.split("=").let { mapValue ->
                                val type = mapValue[0]
                                val value = mapValue[1].toFloat()
                                when (type) {
                                    Constants.TEMP -> {
                                        Log.d(TAG, "HandlerListener - onReceived - TEMP")
                                        hashMap.putAll(
                                                mSensorComposite.updateSensorData(IoTSensor.SOIL_TEMP.name, value)
                                        )
                                    }
                                    Constants.HUMID -> {
                                        Log.d(TAG, "HandlerListener - onReceived - HUMID")
                                        hashMap.putAll(
                                                mSensorComposite.updateSensorData(IoTSensor.SOIL_HUMID.name, value)
                                        )
                                    }
                                }
                            }
                        }
                    }
                    Constants.PH_SENSOR -> {
                        Log.d(TAG, "HandlerListener - onReceived - PH_SENSOR")
                        content.split(",").forEach {
                            it.split("=").let { mapValue ->
                                val type = mapValue[0]
                                val value = mapValue[1].toFloat()
                                if (type == Constants.PH) {
                                    Log.d(TAG, "HandlerListener - onReceived - PH")
                                    hashMap.putAll(
                                            mSensorComposite.updateSensorData(IoTSensor.SOIL_PH.name, value)
                                    )
                                }
                            }
                        }
                    }
                    Constants.EC_SENSOR -> {
                        Log.d(TAG, "HandlerListener - onReceived - EC_SENSOR")
                        content.split(",").forEach {
                            it.split("=").let { mapValue ->
                                val type = mapValue[0]
                                val value = mapValue[1].toFloat()
                                if (type == Constants.EC) {
                                    Log.d(TAG, "HandlerListener - onReceived - EC")
                                    hashMap.putAll(
                                            mSensorComposite.updateSensorData(IoTSensor.SOIL_EC.name, value)
                                    )
                                }
                            }
                        }
                    }
                }
                if (hashMap.isNotEmpty()) {
                    mListenerList.forEach {
                        if (it is SensorDataChangedListener) it.onSensorDataChanged(hashMap)
                    }
                }
                Log.d(TAG, "HandlerListener - onReceived - END")
            }
        }

        override fun onConnect(clientId: String, username: String) {
            Log.d(TAG, "HandlerListener - onConnect - START")
            mIotKitConnectionList[clientId] = ConnectionStatus.CONNECT
            mListenerList.forEach {
                if (it is SensorConnectionListener) it.onSensorConnect(clientId, username)
            }
            Log.d(TAG, "HandlerListener - onConnect - END")
        }

        override fun onDisconnect(clientId: String, username: String) {
            Log.d(TAG, "HandlerListener - onDisconnect - START")
            mIotKitConnectionList[clientId] = ConnectionStatus.DISCONNECT
            mListenerList.forEach {
                if (it is SensorConnectionListener) it.onSensorDisconnect(clientId, username)
            }
            Log.d(TAG, "HandlerListener - onDisconnect - END")
        }

        override fun onConnectionLost(clientId: String, username: String) {
            Log.d(TAG, "HandlerListener - onConnectionLost - START")
            mIotKitConnectionList[clientId] = ConnectionStatus.CONNECTION_LOST
            mListenerList.forEach {
                if (it is SensorConnectionListener) it.onSensorConnectionLost(clientId, username)
            }
            Log.d(TAG, "HandlerListener - onConnectionLost - END")
        }
    })

    fun startSensorListener(listener: SensorListener) {
        mListenerList.add(listener)
    }

    fun stopSensorListener(listener: SensorListener) {
        mListenerList.remove(listener)
    }

    fun startServer() {
        broker.addUserHandler(mMQTTBrokerListener)
        broker.startBroker()
    }

    fun stopServer() {
        broker.stopBroker()
        broker.removeUserHandler(mMQTTBrokerListener)
    }

    interface SensorListener

    interface SensorDataChangedListener : SensorListener {
        fun onSensorDataChanged(sensorData: HashMap<String, Float>)
    }

    interface SensorConnectionListener : SensorListener {
        fun onSensorConnect(clientId: String, username: String)
        fun onSensorDisconnect(clientId: String, username: String)
        fun onSensorConnectionLost(clientId: String, username: String)
    }
}