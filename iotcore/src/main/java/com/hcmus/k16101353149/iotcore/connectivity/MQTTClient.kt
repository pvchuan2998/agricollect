package com.hcmus.k16101353149.iotcore.connectivity

import android.util.Log
import com.hcmus.k16101353149.iotcore.Constants
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import kotlin.math.round
import kotlin.random.Random

class MQTTClient {

    companion object {
        private val TAG = MQTTClient::class.java.simpleName

        fun testAirTemperatureHumiditySensor() {
            Log.d(TAG, "testAirTemperatureHumiditySensor - START")
            /**
             * Cảm biến độ ẩm, nhiệt độ không khí SHT30 Temperature Humidity Sensor V3
             * Cảm biến phía trong: SHT30
             * Điện áp sử dụng: 2.15~5.5VDC
             * Khoảng nhiệt độ đo được: -40 ~ 125 độ C, sai số 0.2 độ C
             * Khoảng độ ẩm đo được: 0 ~100% RH, sai số 2% RH.
             * Có sẵn trở treo 10K và tụ lọc nhiễu.
             * Độ dài cảm biến: 68mm
             * Độ dài dây dẫn: 50cm
             **/
            val temp = ((round(Random.nextDouble(10.0, 30.0) * 10)) / 10).toFloat()
            val humid = ((round(Random.nextDouble(40.0, 100.0) * 10)) / 10).toFloat()
            val topic = Constants.AIR_SENSOR
            val content = "TEMP=$temp,HUMID=$humid"
            val broker = Constants.BROKER_DEFAULT
            val clientId = Constants.AIR_SENSOR_ID

            Log.d(TAG, "testAirTemperatureHumiditySensor - topic=$topic, content=$content, broker=$broker, clientId=$clientId")
            testSendMsg(topic = topic, content = content, broker = broker, clientId = clientId)
            Log.d(TAG, "testAirTemperatureHumiditySensor - END")
        }

        fun testSoilTemperatureHumiditySensor() {
            Log.d(TAG, "testSoilTemperatureHumiditySensor - START")
            /**
             * Cảm biến độ ẩm, nhiệt độ đất SHT30 có vỏ bảo vệ V4
             * Cảm biến phía trong: SHT30
             * Điện áp sử dụng: 2.15~5.5VDC
             * Khoảng nhiệt độ đo được: -40 ~ 125 độ C, sai số 0.2 độ C
             * Khoảng độ ẩm đo được: 0 ~100% RH, sai số 2% RH.
             * Có sẵn trở treo 10K và tụ lọc nhiễu.
             * Độ dài cảm biến: 44mm
             * Độ dài dây dẫn: 50cm
             **/
            val temp = ((round(Random.nextDouble(10.0, 30.0) * 10)) / 10).toFloat()
            val humid = ((round(Random.nextDouble(40.0, 100.0) * 10)) / 10).toFloat()
            val topic = Constants.SOIL_SENSOR
            val content = "TEMP=$temp,HUMID=$humid"
            val broker = Constants.BROKER_DEFAULT
            val clientId = Constants.SOIL_SENSOR_ID

            Log.d(TAG, "testSoilTemperatureHumiditySensor - topic=$topic, content=$content, broker=$broker, clientId=$clientId")
            testSendMsg(topic = topic, content = content, broker = broker, clientId = clientId)
            Log.d(TAG, "testSoilTemperatureHumiditySensor - END")
        }

        fun testSoilPowerOfHydrogenSensor() {
            Log.d(TAG, "testSoilPowerOfHydrogenSensor - START")
            /**
             * Cảm biến độ ẩm, nhiệt độ đất SHT30 có vỏ bảo vệ V4
             * Cảm biến phía trong: SHT30
             * Điện áp sử dụng: 2.15~5.5VDC
             * Khoảng nhiệt độ đo được: -40 ~ 125 độ C, sai số 0.2 độ C
             * Khoảng độ ẩm đo được: 0 ~100% RH, sai số 2% RH.
             * Có sẵn trở treo 10K và tụ lọc nhiễu.
             * Độ dài cảm biến: 44mm
             * Độ dài dây dẫn: 50cm
             **/
            val pH = ((round(Random.nextDouble(4.0, 9.0) * 10)) / 10).toFloat()
            val topic = Constants.PH_SENSOR
            val content = "PH=$pH"
            val broker = Constants.BROKER_DEFAULT
            val clientId = Constants.PH_SENSOR_ID

            Log.d(TAG, "testSoilPowerOfHydrogenSensor - topic=$topic, content=$content, broker=$broker, clientId=$clientId")
            testSendMsg(topic = topic, content = content, broker = broker, clientId = clientId)
            Log.d(TAG, "testSoilPowerOfHydrogenSensor - END")
        }

        fun testSoilElectricalConductivitySensor() {
            Log.d(TAG, "testSoilElectricalConductivitySensor - START")
            /**
             * Cảm biến độ ẩm, nhiệt độ đất SHT30 có vỏ bảo vệ V4
             * Cảm biến phía trong: SHT30
             * Điện áp sử dụng: 2.15~5.5VDC
             * Khoảng nhiệt độ đo được: -40 ~ 125 độ C, sai số 0.2 độ C
             * Khoảng độ ẩm đo được: 0 ~100% RH, sai số 2% RH.
             * Có sẵn trở treo 10K và tụ lọc nhiễu.
             * Độ dài cảm biến: 44mm
             * Độ dài dây dẫn: 50cm
             **/
            val eC = ((round(Random.nextDouble(0.0, 20.0) * 10)) / 10).toFloat()
            val topic = Constants.EC_SENSOR
            val content = "EC=$eC"
            val broker = Constants.BROKER_DEFAULT
            val clientId = Constants.EC_SENSOR_ID

            Log.d(TAG, "testSoilElectricalConductivitySensor - topic=$topic, content=$content, broker=$broker, clientId=$clientId")
            testSendMsg(topic = topic, content = content, broker = broker, clientId = clientId)
            Log.d(TAG, "testSoilElectricalConductivitySensor - END")
        }

        private fun testSendMsg(topic: String, content: String, qos: Int = 2, broker: String, clientId: String) {
            Log.d(TAG, "testSendMsg - START")
            Log.d(TAG, "testSendMsg - topic=$topic, content=$content, qos=$qos, broker=$broker, clientId=$clientId")
            try {
                val client = MqttClient(broker, clientId, MemoryPersistence())
                val connOpts = MqttConnectOptions()
                connOpts.isCleanSession = true
                Log.d(TAG, "testSendMsg - clientId=$clientId connecting to broker: $broker")
                client.connect(connOpts)
                Log.d(TAG, "testSendMsg - clientId=$clientId connected to broker")
                Log.d(TAG, "testSendMsg - clientId=$clientId publishing message: $content")
                val message = MqttMessage(content.toByteArray())
                message.qos = qos
                client.publish(topic, message)
                Log.d(TAG, "testSendMsg - clientId=$clientId message published")
                client.disconnect()
                Log.d(TAG, "testSendMsg - clientId=$clientId disconnected")
            } catch (e: MqttException) {
                e.printStackTrace()
            }
            Log.d(TAG, "testSendMsg - END")
        }
    }
}