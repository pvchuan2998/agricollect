package com.hcmus.k16101353149.iotcore.sensor

enum class IoTSensor : SensorComponent {
    AIR_HUMID {
        override var mValue: Float = 0f
            set(value) {
                if (value in 0f..100f) field = value
            }
    },
    AIR_TEMP {
        override var mValue: Float = 0f
            set(value) {
                if (value in -40f..125f) field = value
            }
    },
    SOIL_HUMID {
        override var mValue: Float = 0f
            set(value) {
                if (value in 0f..100f) field = value
            }
    },
    SOIL_TEMP {
        override var mValue: Float = 0f
            set(value) {
                if (value in -40f..125f) field = value
            }
    },
    SOIL_PH {
        override var mValue: Float = 0f
            set(value) {
                if (value in 0f..14f) field = value
            }
    },
    SOIL_EC {
        override var mValue: Float = 0f
            set(value) {
                if (value in 1f..20f) field = value
            }
    };

    override fun getMeasure(): HashMap<String, Float> {
        return hashMapOf(name to mValue)
    }
}