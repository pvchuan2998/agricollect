package com.hcmus.k16101353149.iotcore

class Constants {

    companion object {

        const val BROKER_DEFAULT = "tcp://0.0.0.0:1883"

        const val SOIL_SENSOR = "soilSensor"
        const val SOIL_SENSOR_ID = "SOIL TEMP&HUMID KIT"

        const val AIR_SENSOR = "airSensor"
        const val AIR_SENSOR_ID = "AIR TEMP&HUMID KIT"

        const val PH_SENSOR = "phSensor"
        const val PH_SENSOR_ID = "PH KIT"

        const val EC_SENSOR = "ecSensor"
        const val EC_SENSOR_ID = "EC KIT"

        const val TEMP = "TEMP"
        const val HUMID = "HUMID"
        const val PH = "PH"
        const val EC = "EC"
    }
}