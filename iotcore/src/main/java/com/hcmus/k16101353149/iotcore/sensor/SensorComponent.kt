package com.hcmus.k16101353149.iotcore.sensor

internal interface SensorComponent {
    var mValue: Float
    fun getMeasure(): HashMap<String, Float>
}