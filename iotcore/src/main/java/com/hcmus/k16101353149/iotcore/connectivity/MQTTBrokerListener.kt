package com.hcmus.k16101353149.iotcore.connectivity

import android.util.Log
import io.moquette.interception.AbstractInterceptHandler
import io.moquette.interception.messages.*
import java.nio.charset.Charset

class MQTTBrokerListener(private val mListener: HandlerListener?) : AbstractInterceptHandler() {

    companion object {
        private val TAG = MQTTBrokerListener::class.java.simpleName
    }

    override fun onPublish(msg: InterceptPublishMessage) {
        Log.d(TAG, "onPublish - START")
        super.onPublish(msg)
        val length = msg.payload.readableBytes()
        val content = msg.payload.toString(0, length, Charset.defaultCharset())

        Log.d(TAG, "onPublish - clientID=${msg.clientID}, username=${msg.username}, topic=${msg.topicName}, content=$content")
        mListener?.onReceived(msg.clientID, msg.username ?: "", msg.topicName, content)

        Log.d(TAG, "onPublish - END")
    }

    override fun onConnect(msg: InterceptConnectMessage?) {
        Log.d(TAG, "onConnect - START")
        super.onConnect(msg)
        msg?.let {
            Log.d(TAG, "onConnect - clientID=${it.clientID}, username=${it.username}, isCleanSession=${it.isCleanSession}, isPasswordFlag=${it.isPasswordFlag}, " +
                    "isUserFlag=${it.isUserFlag}, isWillFlag=${it.isWillFlag}, isWillRetain=${it.isWillRetain}, keepAlive=${it.keepAlive}, protocolName=${it.protocolName}, " +
                    "protocolVersion=${it.protocolVersion.toInt()}, willQos=${it.willQos.toInt()}, willTopic=${it.willTopic}, isDupFlag=${it.isDupFlag}, isRetainFlag=${it.isRetainFlag}, qos=${it.qos}")
            mListener?.onConnect(msg.clientID, msg.username ?: "")
        }
        Log.d(TAG, "onConnect - END")
    }

    override fun onDisconnect(msg: InterceptDisconnectMessage?) {
        Log.d(TAG, "onDisconnect - START")
        super.onDisconnect(msg)
        msg?.let {
            Log.d(TAG, "onDisconnect - clientID=${it.clientID}, username=${it.username}")
            mListener?.onDisconnect(msg.clientID, msg.username ?: "")
        }
        Log.d(TAG, "onDisconnect - END")
    }

    override fun onConnectionLost(msg: InterceptConnectionLostMessage?) {
        Log.d(TAG, "onConnectionLost - START")
        super.onConnectionLost(msg)
        msg?.let {
            Log.d(TAG, "onConnectionLost - clientID=${it.clientID}, username=${it.username}")
            mListener?.onConnectionLost(msg.clientID, msg.username ?: "")
        }
        Log.d(TAG, "onConnectionLost - END")
    }

    override fun onSubscribe(msg: InterceptSubscribeMessage?) {
        Log.d(TAG, "onSubscribe - START")
        super.onSubscribe(msg)
        msg?.let {
            Log.d(TAG, "onSubscribe - clientID=${it.clientID}, username=${it.username}, requestedQos=${it.requestedQos}, topicFilter=${it.topicFilter}")
        }
        Log.d(TAG, "onSubscribe - END")
    }

    override fun onUnsubscribe(msg: InterceptUnsubscribeMessage?) {
        Log.d(TAG, "onUnsubscribe - START")
        super.onUnsubscribe(msg)
        msg?.let {
            Log.d(TAG, "onUnsubscribe - clientID=${it.clientID}, username=${it.username}, topicFilter=${it.topicFilter}")
        }
        Log.d(TAG, "onUnsubscribe - END")
    }

    override fun onMessageAcknowledged(msg: InterceptAcknowledgedMessage?) {
        Log.d(TAG, "onMessageAcknowledged - START")
        super.onMessageAcknowledged(msg)
        msg?.let {
            Log.d(TAG, "onMessageAcknowledged - username=${it.username}, packetID=${it.packetID}, topic=${it.topic}")
        }
        Log.d(TAG, "onMessageAcknowledged - END")
    }

    override fun getID(): String {
        return "1"
    }

    interface HandlerListener {
        fun onReceived(clientId: String, username: String, topic: String, content: String)
        fun onConnect(clientId: String, username: String)
        fun onDisconnect(clientId: String, username: String)
        fun onConnectionLost(clientId: String, username: String)
    }
}