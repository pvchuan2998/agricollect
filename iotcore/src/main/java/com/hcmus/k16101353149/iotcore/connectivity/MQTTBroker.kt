package com.hcmus.k16101353149.iotcore.connectivity

import android.util.Log
import io.moquette.broker.Server
import io.moquette.broker.config.MemoryConfig
import io.moquette.interception.InterceptHandler
import java.util.*

class MQTTBroker {

    companion object {
        private val TAG = MQTTBroker::class.java.simpleName
    }

    private val mProps = Properties()
    private val mConfigs = MemoryConfig(mProps)
    private val mMqttBroker = Server()
    private val mUserHandlers = arrayListOf<InterceptHandler>()

    fun startBroker() {
        Log.d(TAG, "startBroker - START")
        mMqttBroker.startServer(mConfigs, mUserHandlers)
        Log.d(TAG, "startBroker - END")
    }

    fun stopBroker() {
        Log.d(TAG, "stopBroker - START")
        mMqttBroker.stopServer()
        Log.d(TAG, "stopBroker - END")
    }

    fun addUserHandler(userHandler: InterceptHandler) {
        Log.d(TAG, "addUserHandler - START")
        mUserHandlers.add(userHandler)
        Log.d(TAG, "addUserHandler - END")
    }

    fun removeUserHandler(userHandler: InterceptHandler) {
        Log.d(TAG, "removeUserHandler - START")
        mUserHandlers.remove(userHandler)
        Log.d(TAG, "removeUserHandler - END")
    }
}