package com.hcmus.k16101353149.iotcore.sensor

import android.util.Log

internal class SensorComposite(listSupportedSensor: List<IoTSensor>) : SensorComponent {

    companion object {
        private val TAG = SensorComposite::class.java.simpleName
    }

    override var mValue: Float = 0f

    private val mSupportedSensors: HashMap<String, SensorComponent> = LinkedHashMap()

    init {
        Log.d(TAG, "Init - START - listSupportedSensor=$listSupportedSensor")
        listSupportedSensor.forEach {
            mSupportedSensors[it.name] = it
        }
        Log.d(TAG, "Init - END")
    }

    fun updateSensorData(name: String, value: Float): HashMap<String, Float> {
        Log.d(TAG, "updateSensorData - START - name=$name, value=$value")
        val hashMap = HashMap<String, Float>()
        mSupportedSensors[name]?.let {
            Log.d(TAG, "updateSensorData - update name=$name, mValue=${it.mValue} to value=$value")
            it.mValue = value
            Log.d(TAG, "updateSensorData - after update data=${it.getMeasure()}")
            hashMap.putAll(it.getMeasure())
        }
        Log.d(TAG, "updateSensorData - END")
        return hashMap
    }

    override fun getMeasure(): HashMap<String, Float> {
        Log.d(TAG, "getMeasure - START")
        val hashMap = HashMap<String, Float>()
        mSupportedSensors.forEach {
            hashMap.putAll(it.value.getMeasure())
        }
        Log.d(TAG, "getMeasure - END - return hashMap=$hashMap")
        return hashMap
    }
}