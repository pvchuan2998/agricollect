package com.hcmus.k16101353149.iotcore.connectivity

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.wifi.WifiManager
import android.os.RemoteException
import android.util.Log
import kotlinx.coroutines.*
import java.io.IOException
import java.net.*

class IotKitConnectivity(context: Context) {

    companion object {
        private const val TAG = "IotKitConnectivity"

        private const val GROUP_IP_ADDRESS = "224.168.1.1"
        private const val GROUP_PORT = 2020
    }

    private val mWifiManager: WifiManager? = context.getSystemService(WIFI_SERVICE) as WifiManager?
    private var mGroupIP: InetAddress? = null
    private var mSocket: MulticastSocket? = null
    private var mIsSendingMulticastLocalIP = false

    fun startSendingMulticastLocalIP(): Boolean {
        Log.d(TAG, "startSendingMulticastLocalIP")
        var result = false
        try {
            mGroupIP = InetAddress.getByName(GROUP_IP_ADDRESS)
            try {
                mSocket = MulticastSocket(GROUP_PORT)
                try {
                    mSocket?.let {
                        it.joinGroup(mGroupIP)
                        mIsSendingMulticastLocalIP = true
                        sendMulticastLocalIP()
                        result = true
                    }
                } catch (e: SocketException) {
                    Log.e(TAG, "sendMulticastPacket - e=$e")
                    mSocket = null
                    mGroupIP = null
                }
            } catch (e: IOException) {
                Log.e(TAG, "sendMulticastPacket - e=$e")
                mGroupIP = null
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, "sendMulticastPacket - e=$e")
        }
        return result
    }

    fun stopSendingMulticastLocalIP() {
        Log.d(TAG, "stopSendingMulticastLocalIP")
        mIsSendingMulticastLocalIP = false
    }

    private fun sendMulticastLocalIP() {
        Log.d(TAG, "sendMulticastLocalIP")
        CoroutineScope(Dispatchers.IO).launch {
            mSocket?.let {
                val ip = getWiFiIpAddress()
                if (ip.isNotEmpty()) {
                    val buff: ByteArray = ip.toByteArray()
                    val packet = DatagramPacket(buff, buff.size, mGroupIP, GROUP_PORT)
                    while (mIsSendingMulticastLocalIP) {
                        try {
                            withContext(Dispatchers.IO) {
                                Log.d(TAG, "sendMulticastLocalIP - send")
                                it.send(packet)
                            }
                        } catch (e: IOException) {
                            Log.e(TAG, "sendMulticastLocalIP - e=$e")
                        }
                        delay(500L)
                    }
                    try {
                        withContext(Dispatchers.IO) {
                            Log.d(TAG, "sendMulticastLocalIP - stop send")
                            it.leaveGroup(mGroupIP)
                        }
                    } catch (e: IOException) {
                        Log.e(TAG, "sendMulticastLocalIP - e=$e")
                    }
                }
            }
            mSocket = null
            mGroupIP = null
        }
    }

    private fun getWiFiIpAddress(): String {
        return mWifiManager?.let {
            try {
                val ip = it.connectionInfo.ipAddress
                String.format("${ip.and(0xff)}." +
                        "${ip.shr(8).and(0xff)}." +
                        "${ip.shr(16).and(0xff)}." +
                        "${ip.shr(24).and(0xff)}")
            } catch (e: RemoteException) {
                Log.e(TAG, "getWiFiIpAddress - e=$e")
                ""
            }
        } ?: ""
    }
}