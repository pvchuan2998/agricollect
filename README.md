# AgriCollect 

## Key features:
1. Login/logout
2. Push notifications when temperature/humidity... goes under the allowed threshold
3. Realtime plant's deseases detector
4. Collect environmental parameters such as (temp, humid, EC...) with IOT kit



Hướng dẫn sử dụng iotcore cho tầng app UI

Biến member
```
    private val mListSupportedSensor = arrayListOf(
            IoTSensor.AIR_HUMID,
            IoTSensor.AIR_TEMP,
            IoTSensor.SOIL_HUMID,
            IoTSensor.SOIL_TEMP
    )
    private val mSensorManager = SensorManager(mListSupportedSensor)
    private val mSensorManagerListener = object : SensorManager.SensorManagerListener {
        override fun onSensorDataChanged(sensorData: HashMap<String, Float>) {
            Log.d("RegisterActivity", "onSensorDataChanged - START - sensorData=$sensorData")
        }
    }
```

Nên đặt theo scope của activity life cycle
Đăng kí lắng nghe IoT Sensor
```
    mSensorManager.startSensorListener(mSensorManagerListener)
    MQTTClient.testAirTemperatureHumiditySensor()
    MQTTClient.testSoilTemperatureHumiditySensor()
```

Hủy đăng kí lắng nghe IoT Sensor
```
    mSensorManager.stopSensorListener()
```
